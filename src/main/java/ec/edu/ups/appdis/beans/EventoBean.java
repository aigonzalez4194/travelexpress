package ec.edu.ups.appdis.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import org.primefaces.event.RowEditEvent;
import com.lowagie.text.pdf.codec.Base64;

import ec.edu.ups.appdis.bussiness.EventoBussiness;
import ec.edu.ups.appdis.bussiness.PagoBussiness;
import ec.edu.ups.appdis.bussiness.RentaBussiness;
import ec.edu.ups.appdis.bussiness.TipoBussiness;
import ec.edu.ups.appdis.model.Auto;
import ec.edu.ups.appdis.model.Evento;
import ec.edu.ups.appdis.model.Pago;
import ec.edu.ups.appdis.model.Renta;
import ec.edu.ups.appdis.model.Tipo_Pago;



@Named("bean_evento")
//@ViewScoped
@ViewScoped
public class EventoBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private EventoBussiness eventoBussiness;
	
	@Inject 
	private PagoBussiness pagoBussiness;
	private Pago newPago;
	
	@Inject
	private FacesContext context;
	
	@Inject
	private Sesion sesion;

	/*
	 * Instancia del Bean Auto
	 */
	@SuppressWarnings("deprecation")
	@ManagedProperty(value="#{bean_auto}")
	private AutoBean autoBean;
	
	/*
	 * Instancia del Bean Pago
	 */
	@SuppressWarnings("deprecation")
	@ManagedProperty(value="#{bean_pago}")
	private PagoBean pagoBean;
	
		
	private Evento newEvento;
	private Renta selectRenta;
	private List<Renta> renta;
	private Date fecha;
	
	//private int numeroDiasRenta;
	
	/*
	 * Variables para cargar el tipo de pago al Combo, Pago
	 */
	private List<Tipo_Pago> listaTipoPago;
	private List<SelectItem> listadoTipoPagoCombo;
	private String selecionTipoPago;
	
	@Inject
	private TipoBussiness tpagoBussiness;
	
//	 /**
//	 * listas y campos para manejar los combos
//	 */
//	private List<SelectItem> listaHoraRenta;
//	private String lhr;
//	
//	private List<SelectItem> listaMinutosRenta;
//	private String lmr;
	
	/**
	 * Para imagen del auto
	 */
	private Part file;
	
	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	@PostConstruct
	public void init() {
		
		newEvento =new Evento ();
		newPago = new Pago();
//		renta = rentaBussiness.getListadoRentas();	
		
//		cargarHoraRenta();
//		cargarMinutos();
		
		cargarSeleccionTipoPago();
	}


	public String cancelarActualizacion() {
		return "Evento.xhtml?faces-redirect=true";
	}
	
	/**
	 * Metodo para cargar (tipoPago) a la lista del combo.
	 */
	public void cargarSeleccionTipoPago (){
		listadoTipoPagoCombo = new ArrayList<SelectItem>();
		listaTipoPago=tpagoBussiness.getListadoTPago();
		for(int i=0; i<listaTipoPago.size(); i++){
			listadoTipoPagoCombo.add(new SelectItem(listaTipoPago.get(i).getCodigo(),listaTipoPago.get(i).getDescripcion()));
//			System.out.println("--------Carga categorias opcion--------");
		}
	}
	
	
	public void seleccionarTipoPagoCombo(){
		try {
			for (int i=0; i<listaTipoPago.size(); i++){
				if((""+listaTipoPago.get(i).getCodigo()).equals(selecionTipoPago)){
					newPago.setTpago(listaTipoPago.get(i));

					System.out.println("Codigo tipoPago: "+ listaTipoPago.get(i).getCodigo()+ " ==> "+ "Nombre: "+listaTipoPago.get(i).getDescripcion());
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public String guardarPago() {
		
		try {
			pagoBussiness.save(newPago);
			
			System.out.println("Registro satisfactorio");
			
			return "";
		} catch (Exception e) {
			System.out.println("Error al guardar pago");
		}
		return null;
	}
	
	 /**
	  * Metodo para redondear Valor a Pagar
	  * @param numero
	  * @return
	  */
	public  double redondear(double numero){
       // return Math.rint(numero*100)/100;
       return Math.round(numero*100.0)/100.0;
    }
	
	/**
	 * Metodo para guardar una renta, realizada por el Usuario
	 * @return
	 */
	public String guardar(){	
		try {
//			newPago.setValorTotal(redondear(sesion.getValorTotal()));
			pagoBussiness.guardar(newPago);
			//Integer.parseInt(autoBean.getUsuarioSession()) ;
			System.out.println("///////// ///////"+ sesion.getAuto().getModelo());
			
			System.out.println("///////////////// Dato del usuario /// //////////////"+sesion.getCliente().getNombres());
			
			System.out.println("Valor total a pagar RENTA: "+ sesion.getValorTotal()+"..............");
			newEvento.setHora(sesion.getHoraGlobal());
			newEvento.setCliente(sesion.getCliente());
			newEvento.setFecha(sesion.getFechaGlobal());		
			newEvento.setAuto(sesion.getAuto());
			newEvento.setChofer(sesion.getChofer());
			newEvento.setPago(newPago); //Seteo el pago del Evento
//			
			eventoBussiness.save(newEvento);
//			
//			
	        FacesMessage msg = new FacesMessage("Aviso","Su renta ha sido registrado satisfactoriamente");
	        context.addMessage(null, msg);
	        //FacesContext.getCurrentInstance().addMessage(null, msg);
			System.out.println("Registro Guardado");
					
		} catch (Exception e) {
			System.out.println("Error al guardar"); 
			 FacesMessage msg = new FacesMessage("Error","No se puede realizar la renta");
	         //FacesContext.getCurrentInstance().addMessage(null, msg);
	         context.addMessage(null, msg);
			e.printStackTrace();
		}	
		return "Evento.xhtml?faces-redirect=true";	
	}

	public String pagarRenta() {
		
		return "pagoRenta.xhtml?faces-redirect=true";
	}
	
//	public String eliminar(int cod) {
//		try {
//			rentaBussiness.eliminar(cod);
//			init();
//			FacesMessage msg = new FacesMessage("Aviso","Renta eliminada"+cod);
//	        FacesContext.getCurrentInstance().addMessage(null, msg);
//		} catch (Exception e) {
//			e.printStackTrace();
//			FacesMessage msg = new FacesMessage("Error","El registro no ha sido eliminado");
//	        FacesContext.getCurrentInstance().addMessage(null, msg);
//		}
//		return null;
//	}
//	
//	
//	private void cargarHoraRenta(){
//		listaHoraRenta = new ArrayList<SelectItem>();
//		listaHoraRenta   .add(new SelectItem("00"));
//		listaHoraRenta   .add(new SelectItem("01"));
//		listaHoraRenta   .add(new SelectItem("02"));
//		listaHoraRenta   .add(new SelectItem("03"));
//		listaHoraRenta   .add(new SelectItem("04"));
//		listaHoraRenta   .add(new SelectItem("05"));
//		listaHoraRenta   .add(new SelectItem("06"));
//		listaHoraRenta   .add(new SelectItem("07"));
//		listaHoraRenta   .add(new SelectItem("08"));
//		listaHoraRenta   .add(new SelectItem("09"));
//		listaHoraRenta   .add(new SelectItem("10"));
//		listaHoraRenta   .add(new SelectItem("11"));
//		listaHoraRenta   .add(new SelectItem("12"));
//		listaHoraRenta   .add(new SelectItem("13"));
//		listaHoraRenta   .add(new SelectItem("14"));
//		listaHoraRenta   .add(new SelectItem("15"));
//		listaHoraRenta   .add(new SelectItem("16"));
//		listaHoraRenta   .add(new SelectItem("17"));
//		listaHoraRenta   .add(new SelectItem("18"));
//		listaHoraRenta   .add(new SelectItem("19"));
//		listaHoraRenta   .add(new SelectItem("20"));
//		listaHoraRenta   .add(new SelectItem("21"));
//		listaHoraRenta   .add(new SelectItem("22"));
//		listaHoraRenta   .add(new SelectItem("23"));
//
//	}
//	private void cargarMinutos() {
//		listaMinutosRenta = new ArrayList<SelectItem>();
//		listaMinutosRenta.add(new SelectItem("00"));
//		listaMinutosRenta.add(new SelectItem("15"));
//		listaMinutosRenta.add(new SelectItem("30"));
//		listaMinutosRenta.add(new SelectItem("45"));
//	}
//	
	
	/**
	 * Metodo para setear la hora selecionada del ComboBox
	 */
//	public void seleccionarHora() {
//		try {
//			if(!lhr.equals("") && !lmr.equals("")) {
//				newRenta.setHora(lhr +":"+ lmr);
//				System.out.println("La hora seleccionada es:"+ lhr +" "+ "Minuto: "+lmr);
//				//String hora= lhr+ ":" + lmr;
//				//return hora;
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//	
//	}
//	
//	public String seleccionarHora() {
//		try {
//			if(!lhr.equals("") && !lmr.equals("")) {
//				//newRenta.setHora(lhr +":"+ lmr);
//				System.out.println("La hora seleccionada es:"+ lhr +" "+ "Minuto: "+lmr);
//				String hora= lhr+ ":" + lmr;
//				sesion.setHoraGlobal(hora); //Seteo la hora para recuperar en la seleccion del Auto
//				return hora;
//			}
//		} catch (Exception e) {
//			// TODO: handle exception
//		}
//		return null;
//	}
//	
	/**
	 * Actualizar datos a nivel de fila (Auto)
	 * @param event
	 * @throws Exception 
	 */
//	 public void onRowEdit(RowEditEvent event) throws Exception {
//		 Re a = (Auto) event.getObject();
//		 auBussiness.actualizar(a);
//         FacesMessage msg = new FacesMessage("Auto actualizado", ""+((Auto) event.getObject()).getModelo());
//         FacesContext.getCurrentInstance().addMessage(null, msg);
//	    }
//	 public void onRowCancel(RowEditEvent event) throws Exception {
//		 Auto a = (Auto) event.getObject();
//	 	 auBussiness.actualizar(a);
//         FacesMessage msg = new FacesMessage("Actualizacion cancelada", ""+((Auto) event.getObject()).getModelo());
//         FacesContext.getCurrentInstance().addMessage(null, msg);
//	    }


	public List<Renta> getRenta() {
		return renta;
	}

	public void setRenta(List<Renta> renta) {
		this.renta = renta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

//	public List<SelectItem> getListaHoraRenta() {
//		return listaHoraRenta;
//	}
//
//	public void setListaHoraRenta(List<SelectItem> listaHoraRenta) {
//		this.listaHoraRenta = listaHoraRenta;
//	}
//
//	public String getLhr() {
//		return lhr;
//	}
//
//	public void setLhr(String lhr) {
//		this.lhr = lhr;
//	}
//
//	public List<SelectItem> getListaMinutosRenta() {
//		return listaMinutosRenta;
//	}
//
//	public void setListaMinutosRenta(List<SelectItem> listaMinutosRenta) {
//		this.listaMinutosRenta = listaMinutosRenta;
//	}
//
//	public String getLmr() {
//		return lmr;
//	}
//
//	public void setLmr(String lmr) {
//		this.lmr = lmr;
//	}

	public Renta getSelectRenta() {
		return selectRenta;
	}

	public void setSelectRenta(Renta selectRenta) {
		this.selectRenta = selectRenta;
	}

	public Pago getNewPago() {
		return newPago;
	}

	public void setNewPago(Pago newPago) {
		this.newPago = newPago;
	}

	public List<Tipo_Pago> getListaTipoPago() {
		return listaTipoPago;
	}

	public void setListaTipoPago(List<Tipo_Pago> listaTipoPago) {
		this.listaTipoPago = listaTipoPago;
	}

	public List<SelectItem> getListadoTipoPagoCombo() {
		return listadoTipoPagoCombo;
	}

	public void setListadoTipoPagoCombo(List<SelectItem> listadoTipoPagoCombo) {
		this.listadoTipoPagoCombo = listadoTipoPagoCombo;
	}

	public String getSelecionTipoPago() {
		return selecionTipoPago;
	}

	public void setSelecionTipoPago(String selecionTipoPago) {
		this.selecionTipoPago = selecionTipoPago;
	}

	public Evento getNewEvento() {
		return newEvento;
	}

	public void setNewEvento(Evento newEvento) {
		this.newEvento = newEvento;
	}
	
	
}
