package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.AutoDao;
import ec.edu.ups.appdis.model.Auto;


@Stateless
public class AutoBussiness {
	

	@Inject
	private AutoDao cliDao;
	private List<Auto> persona;
	
	
	public boolean save(Auto cliente)throws Exception {
		
		Auto cli=cliDao.read(cliente.getCodigo());
		if (cli!=null) 
			cliDao.update(cliente);
		else 
			
			cliDao.insert(cliente);
			
		return true;
		
	}
	
	public List<Auto>getListadoAutos(){
		
		return cliDao.getAutos();
	}
	
	public List<Auto> getCliente() {
		return persona;
	}

	
	public void eliminar (int cod) throws Exception {
		Auto aux=cliDao.read(cod);       // el aux es igual al per
		if(aux==null)throw new Exception("Persona No Exixte");
		else 
			cliDao.remove(cod);
		
	}
	
	public Auto readAuto(int id) {
		return cliDao.read(id);
	}
	
	public void actualizar(Auto  cliente)throws Exception {
		Auto aux=cliDao.read( cliente.getCodigo());       // el aux es igual al per
		if(aux==null)throw new Exception("Persona No Exixte");
		else 
			cliDao.update( cliente);
	}
	

}
