package ec.edu.ups.appdis.bussiness;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;


import ec.edu.ups.appdis.dao.ViajeDao;

import ec.edu.ups.appdis.model.Viaje;

public class ViajeBussiness implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	private ViajeDao cliDao;
	private List<Viaje> persona;
	
	
	public boolean save(Viaje cliente)throws Exception {
		
		Viaje cli=cliDao.read(cliente.getCodigo_viaje());
		if (cli!=null) 
			cliDao.update(cliente);
		else 
			
			cliDao.insert(cliente);
			
		return true;
		
	}
	
	public List<Viaje>getListadoViajes(){
		
		return cliDao.getViajes();
	}
	
	public List<Viaje> getCliente() {
		return persona;
	}

	
	public void eliminar (int cod) throws Exception {
		Viaje aux=cliDao.read(cod);       // el aux es igual al per
		if(aux==null)throw new Exception("Viaje No Exixte");
		else 
			cliDao.remove(cod);
		
	}
	
	public void actualizar(Viaje  cliente)throws Exception {
		Viaje aux=cliDao.read( cliente.getCodigo_viaje());       // el aux es igual al per
		if(aux==null)throw new Exception("Viaje No Exixte");
		else 
			cliDao.update( cliente);
	}
	

}
