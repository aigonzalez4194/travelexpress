package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;


import ec.edu.ups.appdis.dao.TipoDao;
import ec.edu.ups.appdis.model.Tipo_Pago;

@Stateless
public class TipoBussiness {
	
	@Inject
	private TipoDao cliDao;
	private List<Tipo_Pago> tpago;
	
	
	public boolean save(Tipo_Pago tpago)throws Exception {
		
		Tipo_Pago cli=cliDao.read(tpago.getCodigo());
		if (cli!=null) 
			cliDao.update(tpago);
		else 
			
			cliDao.insert(tpago);
			
		return true;
		
	}
	
	public List<Tipo_Pago>getListadoTPago(){
		
		return cliDao.getTipoPago();
	}
		
	public void eliminar (int cod) throws Exception {
		Tipo_Pago aux=cliDao.read(cod);       // el aux es igual al per
		if(aux==null)throw new Exception("Persona No Exixte");
		else 
			cliDao.remove(cod);
		
	}
	
	public void actualizar(Tipo_Pago  cliente)throws Exception {
		Tipo_Pago aux=cliDao.read( cliente.getCodigo());       // el aux es igual al per
		if(aux==null)throw new Exception("Persona No Exixte");
		else 
			cliDao.update( cliente);
	}
	

}
