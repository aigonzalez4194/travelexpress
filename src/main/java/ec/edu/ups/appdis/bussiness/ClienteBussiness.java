package ec.edu.ups.appdis.bussiness;

import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.ClienteDao;
import ec.edu.ups.appdis.model.Cliente;
import ec.edu.ups.appdis.model.Renta;


@Stateless
public class ClienteBussiness {

	@Inject
	private ClienteDao cliDao;
	private List<Cliente> lisClientes;
	private String mensajeValidaCedula;
		
	public boolean save(Cliente cliente)throws Exception {
		
		Cliente cli=cliDao.read(cliente.getCodigo());
		if (cli!=null) 
			cliDao.update(cliente);
		else  if(cli==null)
			
			cliDao.insert(cliente);
			
		return true;
		
	}
	
	/**
	 * Recupera el Listado de las Personas
	 * @return
	 */
	public List<Cliente>getListadoClientes(){
		return cliDao.getCliente();
	}
	
	public void eliminar (int cod) throws Exception {
		Cliente aux=cliDao.read(cod);       // el aux es igual al per
		if(aux==null)throw new Exception("Cliente No Exixte");
		else 
			cliDao.remove(cod);
		
	}
	
	public void actualizar(Cliente  cliente)throws Exception {
		Cliente aux=cliDao.read( cliente.getCodigo());       // el aux es igual al per
		if(aux==null)throw new Exception("Cliente No Exixte");
		else 
			cliDao.update( cliente);
	}
	
	/**
	 * Metodo para validad cedula
	 * @param cedula
	 * @return
	 */
	public  boolean validaCedula (String x){
	    int suma=0;
	    if(x.length()<=9){
	      System.out.println("Ingrese su cedula de 10 digitos");
	      mensajeValidaCedula="Ingrese su cedula de 10 digitos";
	      return false;
	    }else{
	      int a[]=new int [x.length()/2];
	      int b[]=new int [(x.length()/2)];
	      int c=0;
	      int d=1;
	      for (int i = 0; i < x.length()/2; i++) {
	        a[i]=Integer.parseInt(String.valueOf(x.charAt(c)));
	        c=c+2;
	        if (i < (x.length()/2)-1) {
	          b[i]=Integer.parseInt(String.valueOf(x.charAt(d)));
	          d=d+2;
	        }
	      }
	    
	      for (int i = 0; i < a.length; i++) {
	        a[i]=a[i]*2;
	        if (a[i] >9){
	          a[i]=a[i]-9;	         
	        }
	        suma=suma+a[i]+b[i];
	      } 
	      int aux=suma/10;
	      int dec=(aux+1)*10;
	     
	      if ((dec - suma) == Integer.parseInt(String.valueOf(x.charAt(x.length()-1))))	 {   	  	        
	    	  System.out.println("Su cedula es correcta");
	    	  mensajeValidaCedula="Su cedula es correcta";
	    	  return true;
	      }else	    	 
	        if(suma%10==0 && x.charAt(x.length()-1)=='0'){
	          return true;
	        }else{
	        	System.out.println("Su cedula es incorrecta");
	        	mensajeValidaCedula="Su cedula es incorrecta";
	          return false;
	        }
	    }
	  }

	public String getMensajeValidaCedula() {
		return mensajeValidaCedula;
	}

	public void setMensajeValidaCedula(String mensajeValidaCedula) {
		this.mensajeValidaCedula = mensajeValidaCedula;
	}
	

}
