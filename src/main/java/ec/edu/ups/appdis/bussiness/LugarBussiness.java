package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;


import ec.edu.ups.appdis.dao.LugarDao;

import ec.edu.ups.appdis.model.Lugar;

@Stateless
public class LugarBussiness {

	@Inject
	private LugarDao cliDao;
	private List<Lugar> persona;
	
	
	public boolean save(Lugar cliente)throws Exception {
		
		Lugar cli=cliDao.read(cliente.getCodigo());
		if (cli!=null) 
			cliDao.update(cliente);
		else 
			
			cliDao.insert(cliente);
			
		return true;
		
	}
	
	public List<Lugar>getListadoLugares(){
		return cliDao.getLugares();
	}
	
	public List<Lugar> getCliente() {
		return persona;
	}

	
	public void eliminar (int cod) throws Exception {
		Lugar aux=cliDao.read(cod);       // el aux es igual al per
		if(aux==null)throw new Exception("Lugar No Exixte");
		else 
			cliDao.remove(cod);
		
	}
	
	public void actualizar(Lugar  cliente)throws Exception {
		Lugar aux=cliDao.read( cliente.getCodigo());       // el aux es igual al per
		if(aux==null)throw new Exception("Lugar No Exixte");
		else 
			cliDao.update( cliente);
	}
	
	
	
}
