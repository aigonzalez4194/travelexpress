package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.omg.CORBA.TRANSACTION_UNAVAILABLE;

import ec.edu.ups.appdis.dao.PersonaDao;
import ec.edu.ups.appdis.model.Persona;

@Stateless
public class PersonaBussiness {

	@Inject
	private PersonaDao persDao;
	
	
	public boolean save(Persona persona)throws Exception {
		
		Persona per=persDao.read(persona.getCedula());
		if (per!=null) 
			persDao.update(persona);
		else 
			
			persDao.insert(persona);
			
		return true;
		
	}
	
	public List<Persona>getListadoPersona(){
		
		return persDao.getPersona();
	}
	
	public void eliminar (String cedula) throws Exception {
		Persona aux=persDao.read(cedula);       // el aux es igual al per
		if(aux==null)throw new Exception("Persona No Exixte");
		else 
			persDao.remove(cedula);
		
	}
	
	public void actualizar(Persona persona)throws Exception {
		Persona aux=persDao.read(persona.getCedula());       // el aux es igual al per
		if(aux==null)throw new Exception("Persona No Exixte");
		else 
			persDao.update(persona);
	}
}
