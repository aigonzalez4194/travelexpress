package ec.edu.ups.appdis.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Evento {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	@NotNull
	private Date fecha;
	
	@NotNull
	@Column(length=50)
	private String hora;
	
	/*
	 * Relacion de Tablas con JPA
	 */
	@ManyToOne
	@JoinColumn(name="evento_auto_id", nullable=false)
	@JsonIgnore
	private Auto auto;
	
	@ManyToOne
	@JoinColumn(name="evento_chofer_id", nullable=false)
	@JsonIgnore
	private Chofer chofer;
	
	@ManyToOne
	@JoinColumn(name="evento_cliente_id", nullable=false)
	@JsonIgnore
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name="evento_pago_id", nullable=false)
	@JsonIgnore
	private Pago pago;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public Auto getAuto() {
		return auto;
	}

	public void setAuto(Auto auto) {
		this.auto = auto;
	}

	public Chofer getChofer() {
		return chofer;
	}

	public void setChofer(Chofer chofer) {
		this.chofer = chofer;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Pago getPago() {
		return pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}
	
	
	

}
