package ec.edu.ups.appdis.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;



@Entity
public class Auto implements Serializable{
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	@NotNull
	private String modelo;
	
	@NotNull
	private String placa;
	
	@NotNull
	private int numero_asientos;
	
	@NotNull
	private double precio;
	
	@Lob
	private byte [] imagen_auto;
	
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.EAGER)
	//@OneToMany
	@JoinColumn(name="evento_auto_id", referencedColumnName="codigo")
	private Set<Evento> evento = new HashSet<>();
	
	
	@OneToMany(cascade= {CascadeType.ALL},fetch=FetchType.EAGER)
	//@OneToMany
	@JoinColumn(name="renta_auto_id", referencedColumnName="codigo")
	private Set<Renta> renta = new HashSet<>();
	
	
	@OneToMany(cascade= {CascadeType.ALL},fetch=FetchType.EAGER)
	//@OneToMany
	@JoinColumn(name="viaje_auto_id", referencedColumnName="codigo")
	private Set<Viaje> viaje = new HashSet<>();
	
	
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public byte[] getImagen_auto() {
		return imagen_auto;
	}
	public void setImagen_auto(byte[] imagen_auto) {
		this.imagen_auto = imagen_auto;
	}
	public Set<Evento> getEvento() {
		return evento;
	}
	public void setEvento(Set<Evento> evento) {
		this.evento = evento;
	}
	public int getNumero_asientos() {
		return numero_asientos;
	}
	public void setNumero_asientos(int numero_asientos) {
		this.numero_asientos = numero_asientos;
	}
	public Set<Renta> getRenta() {
		return renta;
	}
	public void setRenta(Set<Renta> renta) {
		this.renta = renta;
	}
	public Set<Viaje> getViaje() {
		return viaje;
	}
	public void setViaje(Set<Viaje> viaje) {
		this.viaje = viaje;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
}
