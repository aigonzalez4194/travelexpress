package ec.edu.ups.appdis.service;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.Id;
import javax.servlet.http.HttpSession;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ec.edu.ups.appdis.bussiness.AutoBussiness;
import ec.edu.ups.appdis.bussiness.ClienteBussiness;
import ec.edu.ups.appdis.dao.AppDaoTE;
import ec.edu.ups.appdis.dao.AutoDao;
import ec.edu.ups.appdis.dao.ClienteDao;
import ec.edu.ups.appdis.model.*;
import ec.edu.ups.appdis.util.Util;

@Path("/personas")

public class PersonaWSREST {
	
	@Inject
	private ClienteBussiness clie;
	
	@Inject
	private AutoBussiness auto;
	
	@Inject
	private AutoDao daoAuto;
	
	
	@Inject
	private ClienteDao daoCliente;
	
	@Inject
	private AppDaoTE daoApp;

	@GET
	@Path("/listaCli")
	@Produces("application/json")
	@Consumes("application/json")
	public List<Cliente> listarClientes(){
		List<Cliente> lista = clie.getListadoClientes();
		return lista;
	}
	
	@GET
	@Path("guardarCliente")
	@Produces("application/json")
	@Consumes("application/json")
	public List<Cliente> guardar (	
			@QueryParam("usuario") String usuario,
			@QueryParam("clave") String clave,
			@QueryParam("cedula") String cedula,
			@QueryParam("nombres") String nombres,
			@QueryParam("apellidos") String apellidos,
			@QueryParam("direccion") String direccion,
			@QueryParam("email") String correo) {	
		
		try {
			List<Cliente>cliente=new ArrayList<>();
			System.out.println("Nombre usuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuario: "+usuario);
			if(!usuario.equals("")&&!clave.equals("")&&!cedula.equals("")&&!nombres.equals("")&&!apellidos.equals("")&&!direccion.equals("")&&!correo.equals("")) {
			Cliente cli = new Cliente();
			cli.setUsuario(usuario);
			cli.setClave(clave);
			cli.setCedula(cedula);
			cli.setNombres(nombres);
			cli.setApellidos(apellidos);
			cli.setDirecion(direccion);
			cli.setEmail(correo);
	
			daoCliente.insert(cli);
			
			cliente.add(cli);
			 return cliente;
			}else {
				 return null;
			}
		} catch (Exception e) {
			return null;
		}
		
		
	}
	
	@GET
	@Path("listaUsuarioLogin")
	@Produces("application/json")
	@Consumes("application/json")
	public List<Cliente> lisUser (@QueryParam("usuario") String usuario){
			try {
				List<Cliente> lista = daoCliente.getUsuario(usuario);
				return lista;
			} catch (Exception e) {
				// TODO: handle exception
			}
		return null;
	}
	
	/**
	 * WS para guardar Renta
	 * @param usuario
	 * @param modelo
	 * @param placa
	 * @param fechR
	 * @param hr
	 * @param numP
	 * @param formP
	 * @param titularT
	 * @param numeroT
	 * @param anioT
	 * @param codigoS
	 * @return
	 */
	@GET
	@Path("guardarRenta")
	@Produces("application/json")
	@Consumes("application/json")
	public List<rentaApp> guardarRenta (	
			@QueryParam("usuario") String usuario,
			@QueryParam("modelo") String modelo,
			@QueryParam("placa") String placa,
			@QueryParam("fechaR") String fechR,
			@QueryParam("hora") String hr,
			@QueryParam("numDias") String numP,
			@QueryParam("formPago") String formP,
			@QueryParam("titularT") String titularT,
			@QueryParam("numeroT") String numeroT,
			@QueryParam("anioT") String anioT,
			@QueryParam("codigoS") String codigoS) {	
		
		try {
			List<rentaApp> renta=new ArrayList<>();
			System.out.println("Nombre usuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuario: "+usuario);
			if(!usuario.equals("")&&!modelo.equals("")&&!placa.equals("")&&!fechR.equals("")&&!hr.equals("")&&!numP.equals("")&&!formP.equals("")&&!titularT.equals("")&&!numeroT.equals("")&&!anioT.equals("")&&!codigoS.equals("")) {
			
			rentaApp ren = new rentaApp();
			ren.setUsuario(usuario);
			ren.setModelo(modelo);
			ren.setPlaca(placa);
			ren.setFechaRenta(fechR);
			ren.setHora(hr);
			ren.setNumDias(numP);
			ren.setFormaPago(formP);
			ren.setTitular(titularT);
			ren.setNumeroT(numeroT);
			ren.setAnio(anioT);
			ren.setCodigoSeguridad(codigoS);
			
			System.out.println(usuario+modelo+placa+fechR+hr+numP+formP+titularT+numeroT+anioT+codigoS);

			daoApp.guardarRenta(ren);
			
			renta.add(ren);
			 return renta;
			}else {
				 return null;
			}
		} catch (Exception e) {
			return null;
		}	
	}
	
	
	/**
	 * WS para guardar Viaje
	 * @param usuario
	 * @param modelo
	 * @param numero
	 * @param costo
	 * @param fecha
	 * @param hora
	 * @param lugar
	 * @param formP
	 * @param titularT
	 * @param numeroT
	 * @param anioT
	 * @param codigoS
	 * @return
	 */
	 
	@GET
	@Path("guardarViaje")
	@Produces("application/json")
	@Consumes("application/json")
	public List<viajeApp> guardarViaje (	
			@QueryParam("usuario") String usuario,
			@QueryParam("modelo") String modelo,
			@QueryParam("numAsientos") String numero,
			@QueryParam("costoViaje") String costo,
			@QueryParam("fecha") String fecha,
			@QueryParam("hora") String hora,
			@QueryParam("lugar") String lugar,
			@QueryParam("formPago") String formP,
			@QueryParam("titularT") String titularT,
			@QueryParam("numeroT") String numeroT,
			@QueryParam("anioT") String anioT,
			@QueryParam("codigoS") String codigoS) {	
		
		try {
			List<viajeApp> renta=new ArrayList<>();
			System.out.println("Nombre usuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuario: "+usuario);
			if(!usuario.equals("")&&!modelo.equals("")&&!numero.equals("")&&!costo.equals("")&&!fecha.equals("")&&!hora.equals("")&&!lugar.equals("")&&!titularT.equals("")&&!numeroT.equals("")&&!anioT.equals("")&&!codigoS.equals("")) {
			
			viajeApp ren = new viajeApp();
			ren.setUsuario(usuario);
			ren.setModelo(modelo);
			ren.setAsientosViaje(numero);
			ren.setValorViaje(costo);
			ren.setHora(hora);
			ren.setFecha(fecha);
			ren.setLugar(lugar);
			ren.setFormaPago(formP);
			ren.setTitularT(titularT);
			ren.setNumeroT(numeroT);
			ren.setAnioT(anioT);
			ren.setCodigoT(codigoS);
			
			System.out.println(usuario+modelo+numero+costo+hora+lugar+formP+titularT+numeroT+anioT+codigoS);

			daoApp.guardarViaje(ren);
			
			renta.add(ren);
			 return renta;
			}else {
				 return null;
			}
		} catch (Exception e) {
			return null;
		}	
	}
//	
	
	
	@GET
	@Path("guardarEvento")
	@Produces("application/json")
	@Consumes("application/json")
	public List<eventoApp> guardarEvento (	
			@QueryParam("usuario") String usuario,
			@QueryParam("modelo") String modelo,
			@QueryParam("fecha") String fecha,
			@QueryParam("numAsientos") String numero,
			@QueryParam("chofer") String chofer,
			@QueryParam("costoViaje") String costo,	
			@QueryParam("hora") String hora,
			@QueryParam("formPago") String formP,
			@QueryParam("titularT") String titularT,
			@QueryParam("numeroT") String numeroT,
			@QueryParam("anioT") String anioT,
			@QueryParam("codigoS") String codigoS) {	
		
		try {
			List<eventoApp> renta=new ArrayList<>();
			System.out.println("Nombre usuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuario: "+usuario);
			if(!usuario.equals("")&&!modelo.equals("")&&!numero.equals("")&&!chofer.equals("")&&!costo.equals("")&&!hora.equals("")&&!fecha.equals("")&&!hora.equals("")&&!titularT.equals("")&&!numeroT.equals("")&&!anioT.equals("")&&!codigoS.equals("")) {
			
			eventoApp ren = new eventoApp();
			ren.setUsuario(usuario);
			ren.setModelo(modelo);
			ren.setFecha(fecha);
			ren.setNumAsientos(numero);
			ren.setChofer(chofer);
			ren.setPrecio(costo);
			ren.setHora(hora);
			ren.setFormaPago(formP);
			ren.setTitularT(titularT);
			ren.setNumeroT(numeroT);
			ren.setAnioT(anioT);
			ren.setCodigoT(codigoS);
			
			//System.out.println(usuario+modelo+numero+costo+hora+lugar+formP+titularT+numeroT+anioT+codigoS);

			daoApp.guardarEvento(ren);
			
			renta.add(ren);
			 return renta;
			}else {
				 return null;
			}
		} catch (Exception e) {
			return null;
		}	
	}

	

	
	
	/**
	 * WS para validar el acceso de usuarios
	 * @param user
	 * @param clave
	 * @return
	 */
	@GET
	@Path("/login")
	@Produces("application/json")
	@Consumes("application/json")
	public Cliente Log (@QueryParam("username") String user, @QueryParam("clave") String clave){
		try {
		
			System.out.println("User: "+user);
			System.out.println("Clave: "+clave);
			System.out.println("Datos consulta inicio sesion: "+daoCliente.loginUser(user, clave));
			Cliente lisUsers = daoCliente.loginUser(user, clave);

			if(lisUsers!=null){
				lisUsers.getUsuario();
				System.out.println("Dato encontrado");		
				return lisUsers;
			}else{	
				System.out.println("Error de consulta");
				return null;		
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}	
	}
	
	/**
	 * WS para listar los autos 
	 * @return
	 */
	@GET
	@Path("/listaAutos")
	@Produces("application/json")
	@Consumes("application/json")
	public List<Auto2> listarAutos(){
		try {
			List<Auto> lista = auto.getListadoAutos();
			List<Auto2> lisAutos = new ArrayList<Auto2>();
			for (int i = 0; i < lista.size(); i++) {
				Auto2 au2 = new Auto2();
				au2.setCodigo(lista.get(i).getCodigo());
				au2.setModelo(lista.get(i).getModelo());
				au2.setPlaca(lista.get(i).getPlaca());
				au2.setNumeroAsientos(lista.get(i).getNumero_asientos());
				au2.setImagen_auto("http://35.211.74.226:8080/TravelExpress/Imagen?ID="+lista.get(i).getCodigo());
				lisAutos.add(au2);
				//System.out.println("Recupera usuariooooooooooooo: "+usuario);
			}
			return lisAutos;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@GET
	@Path("/recibeAuto")
	@Produces("application/json")
	@Consumes("application/json")
	public List<Auto> recibe ( @QueryParam("id") int  codigo, @QueryParam("modelo") String  modelo){
		try {
		System.out.println("Id auto: "+codigo);	
		System.out.println("Modelo auto: "+modelo);	
		List<Auto> listaAuto = daoAuto.recibeAutoSelected(codigo, modelo);
		
		System.out.println("Lista con datos: "+listaAuto.size()); 
		if (listaAuto.size()!=0) {
			listaAuto.get(0).getCodigo();
			
			System.out.println("Auto Seleccionado: "+listaAuto.size());
			return listaAuto;
		}else {
			System.out.println("Error de consulta");
			return null;
		}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
