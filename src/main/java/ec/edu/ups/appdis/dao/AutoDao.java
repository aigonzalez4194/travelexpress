package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.model.Auto;
import ec.edu.ups.appdis.model.Cliente;


@Stateless
public class AutoDao {
	
	@Inject
	private EntityManager em;
	
	public void insert(Auto chofer ) {	
		em.persist(chofer);
		}
	
	public void update(Auto chofer){
		em.merge(chofer);
		
	}
	public void remove(int id){
		em.remove(read(id));
		
	}
	public Auto read(int id){
		Auto aux=em.find(Auto.class, id);
		return aux;
		  
	}
	
	public List<Auto>getAutos(){
		
		String jpaql="Select p from Auto p";	
		Query query=em.createQuery(jpaql,Auto.class);
		List<Auto>listar=query.getResultList();
		return listar;
	}
	
	public List<Auto>recibeAutoSelected(int codigo, String modelo){
		System.out.println("Consulta de auto seleccionado");
		String jpaql="SELECT a from Auto a where a.codigo= :codigo and a.modelo= :modelo ";	
		Query query=em.createQuery(jpaql);
	    query.setParameter("codigo", codigo);
	    query.setParameter("modelo", modelo);
		List<Auto>listar=query.getResultList();
		System.out.println("Datos del auto seleccionado:"+" "+listar.size());
		return listar;
	}

}
