package ec.edu.ups.appdis.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import ec.edu.ups.appdis.bussiness.AutoBussiness;
import ec.edu.ups.appdis.dao.AutoDao;
import ec.edu.ups.appdis.model.Auto;

@Path("/auto")
public class AutoWSRest {

	@Inject
	private AutoBussiness auto;
	
	@Inject
	private AutoDao daoAuto;
	
	
	/**
	 * WS para listar los autos 
	 * @return
	 */
	@GET
	@Path("/listaAutos")
	@Produces("application/json")
	@Consumes("application/json")
	public List<Auto2> listarAutos(){
		try {
			List<Auto> lista = auto.getListadoAutos();
			List<Auto2> lisAutos = new ArrayList<Auto2>();
			for (int i = 0; i < lista.size(); i++) {
				Auto2 au2 = new Auto2();
				au2.setCodigo(lista.get(i).getCodigo());
				au2.setModelo(lista.get(i).getModelo());
				au2.setPlaca(lista.get(i).getPlaca());
				au2.setNumeroAsientos(lista.get(i).getNumero_asientos());
				au2.setImagen_auto("http://192.168.1.210:8080/TravelExpress/Imagen?ID="+lista.get(i).getCodigo());
				lisAutos.add(au2);
				//System.out.println("Recupera usuariooooooooooooo: "+usuario);
			}
			return lisAutos;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@GET
	@Path("/recibeAuto")
	@Produces("application/json")
	@Consumes("application/json")
	public List<Auto> recibe ( @QueryParam("id") int  codigo, @QueryParam("modelo") String  modelo){
		try {
		System.out.println("Id auto: "+codigo);	
		System.out.println("Modelo auto: "+modelo);	
		List<Auto> listaAuto = daoAuto.recibeAutoSelected(codigo, modelo);
		
		System.out.println("Lista con datos: "+listaAuto.size()); 
		if (listaAuto.size()!=0) {
			listaAuto.get(0).getCodigo();
			
			System.out.println("Auto Seleccionado: "+listaAuto.size());
			return listaAuto;
		}else {
			System.out.println("Error de consulta");
			return null;
		}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
