package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;


import ec.edu.ups.appdis.model.Viaje;

public class ViajeDao {
	
	@Inject
	EntityManager em;
	
	public void insert(Viaje cliente ) {		
		em.persist(cliente);		
		}
	
	public void update(Viaje cliente){
		em.merge(cliente);
		
	}
	public void remove(int id){
		em.remove(read(id));
		
	}
	public Viaje read(int id){
		Viaje aux=em.find(Viaje.class, id);
		return aux;
		  
	}
	
	public List<Viaje>getViajes(){
		
		String jpaql="Select p from Viaje p";	
		Query query=em.createQuery(jpaql,Viaje.class);
		List<Viaje>listar=query.getResultList();
		return listar;
	}
	

}
