package ec.edu.ups.appdis.service;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.inject.Inject;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ec.edu.ups.appdis.dao.AutoDao;
import ec.edu.ups.appdis.model.Auto;
@WebServlet("/Imagen")
public class MostrarImagen extends HttpServlet {
	
	@Inject 
	AutoDao daoImagen;
	
	
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response){
		int id=Integer.parseInt(request.getParameter("ID"));
		System.out.println("Este es el id del servlet que le acaba de llegar "+id);
		try {
			Auto au=daoImagen.read(id);
			response.setContentType("image/jpg");
			response.setContentLength(au.getImagen_auto().length);
			response.getOutputStream().write(au.getImagen_auto());
		 
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
