package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.model.Evento;
import ec.edu.ups.appdis.model.Renta;


@Stateless
public class EventoDao {
	
	@Inject
	private EntityManager em;
	
	public void insert(Evento eve ) {	
		em.persist(eve);
		}
	
	public void update(Evento eve){
		em.merge(eve);
		
	}
	public void remove(int id){
		em.remove(read(id));
		
	}
	public Evento read(int id){
		Evento aux=em.find(Evento.class, id);
		return aux;
		  
	}
	
	public List<Evento>getEventos(){
		
		String jpaql="Select p from Evento p";	
		Query query=em.createQuery(jpaql,Evento.class);
		List<Evento>listar=query.getResultList();
		return listar;
	}
	

}
