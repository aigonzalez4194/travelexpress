package ec.edu.ups.appdis.beans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

import org.primefaces.event.RowEditEvent;

import ec.edu.ups.appdis.bussiness.TipoBussiness;
import ec.edu.ups.appdis.model.Cliente;
import ec.edu.ups.appdis.model.Tipo_Pago;


@ManagedBean (name="bean_tpago")
@ViewScoped
public class TipoBean {
	
	@Inject
	private TipoBussiness perBussiness;
	
	@Inject
	private FacesContext facesContext;
	
	private Tipo_Pago newTPago;
	private List<Tipo_Pago> tpagos;
	
	@PostConstruct
	public void init() {
		
		newTPago=new Tipo_Pago();
		tpagos = perBussiness.getListadoTPago();	
	}
	

	public String guardar() {
		try {
			perBussiness.save(newTPago);
			System.out.println("Registro Guardado");
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			 
	            FacesMessage m = new FacesMessage(
	            		FacesMessage.SEVERITY_INFO, "Error!", e.getMessage());
	            facesContext.addMessage(null, m);
			e.printStackTrace();
		}
		
		
		return null;
		
	}
	
	public String Eliminar (int cod )throws Exception {
		try {
			System.out.println("Entro metodo eliminar");
			perBussiness.eliminar(cod);
			init();
			FacesMessage msg = new FacesMessage("Aviso","Un elemento ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Error","El registro no ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}		
		return null;
	}
	
	/**
	 * Actualizar datos a nivel de fila (Tipo de Pago)
	 * @param event
	 * @throws Exception 
	 */
	 public void onRowEdit(RowEditEvent event) throws Exception {
		 Tipo_Pago tp = (Tipo_Pago) event.getObject();
		 perBussiness.actualizar(tp);
         FacesMessage msg = new FacesMessage("Cliente actualizado", ""+((Tipo_Pago) event.getObject()).getDescripcion());
         FacesContext.getCurrentInstance().addMessage(null, msg);
	    }
	 public void onRowCancel(RowEditEvent event) throws Exception {
		Tipo_Pago tp = (Tipo_Pago) event.getObject();
		perBussiness.actualizar(tp);
        FacesMessage msg = new FacesMessage("Actualizacion cancelada", ""+((Tipo_Pago) event.getObject()).getDescripcion());
        FacesContext.getCurrentInstance().addMessage(null, msg);
	    }

	
	public Tipo_Pago getNewTPago() {
		return newTPago;
	}

	public void setNewTPago(Tipo_Pago newTPago) {
		this.newTPago = newTPago;
	}

	public List<Tipo_Pago> getTpagos() {
		return tpagos;
	}

	public void setTpagos(List<Tipo_Pago> tpagos) {
		this.tpagos = tpagos;
	}
	
	

}
