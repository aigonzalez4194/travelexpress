package ec.edu.ups.appdis.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class eventoApp {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	private String usuario;
	
	private String modelo;
	
	private String fecha;
	
	private String numAsientos;
	
	private String chofer;
	
	private String precio;

	private String hora;
	
	private String formaPago;
	
	private String titularT;
	
	private String numeroT;
	
	private String anioT;
	
	private String codigoT;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getNumAsientos() {
		return numAsientos;
	}

	public void setNumAsientos(String numAsientos) {
		this.numAsientos = numAsientos;
	}

	public String getChofer() {
		return chofer;
	}

	public void setChofer(String chofer) {
		this.chofer = chofer;
	}

	public String getPrecio() {
		return precio;
	}

	public void setPrecio(String precio) {
		this.precio = precio;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getTitularT() {
		return titularT;
	}

	public void setTitularT(String titularT) {
		this.titularT = titularT;
	}

	public String getNumeroT() {
		return numeroT;
	}

	public void setNumeroT(String numeroT) {
		this.numeroT = numeroT;
	}

	public String getAnioT() {
		return anioT;
	}

	public void setAnioT(String anioT) {
		this.anioT = anioT;
	}

	public String getCodigoT() {
		return codigoT;
	}

	public void setCodigoT(String codigoT) {
		this.codigoT = codigoT;
	}
	
	
	
	
}
