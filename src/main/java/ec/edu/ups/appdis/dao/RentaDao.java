package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.model.Cliente;
import ec.edu.ups.appdis.model.Renta;


@Stateless
public class RentaDao {
	
	@Inject
	private EntityManager em;
	
	public void insert(Renta chofer ) {	
		em.persist(chofer);
		}
	
	public void update(Renta chofer){
		em.merge(chofer);
		
	}
	public void remove(int id){
		em.remove(read(id));
		
	}
	public Renta read(int id){
		Renta aux=em.find(Renta.class, id);
		return aux;
		  
	}
	
	public List<Renta>getRentas(){
		
		String jpaql="Select p from Renta p";	
		Query query=em.createQuery(jpaql,Renta.class);
		List<Renta>listar=query.getResultList();
		return listar;
	}
	
	public List<Renta> historialCliente(String usuario){
		String jpaql="SELECT DISTINCT r FROM Renta r JOIN r.Cliente c JOIN  r.auto a  WHERE c.usuario='"+usuario+"'";
	                 // "Select f from Frecuencia f join f.destino d join f.cooperaticva c where f.diaSemana = '\"+fechaFrecuencia+\"' and  d.concepto = '\"+destino +\"'\";"                                                               
		Query q=em.createNativeQuery(jpaql, Renta.class);
		q.setParameter("usuario", usuario);
		List<Renta> result=q.getResultList();
		System.out.println("Tamaño lista historial: "+result.size()+".........................//.................");
		return result;
	}
	

}
