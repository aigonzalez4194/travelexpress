package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;


import ec.edu.ups.appdis.model.Tipo_Pago;

@Stateless
public class TipoDao {
	
	@Inject
	private EntityManager em;
	
	public void insert(Tipo_Pago cliente ) {
		
		em.persist(cliente);
		
		}
	
	public void update(Tipo_Pago cliente){
		em.merge(cliente);
		
	}
	public void remove(int id){
		em.remove(read(id));
		
	}
	public Tipo_Pago read(int id){
		Tipo_Pago aux=em.find(Tipo_Pago.class, id);
		return aux;
		  
	}
	
	public List<Tipo_Pago>getTipoPago(){
		
		String jpaql="Select p from Tipo_Pago p";	
		Query query=em.createQuery(jpaql,Tipo_Pago.class);
		List<Tipo_Pago>listar=query.getResultList();
		return listar;
	}
	

}
