package ec.edu.ups.appdis.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Lugar {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	@Column(length=50)
	@NotNull
	private String nombre;
	
	private double precio;
	
	/*
	 * Relacion de uno a varios. Un lugar puede tener uno mas viajes.
	 */
	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="lugar_viaje_id", referencedColumnName="codigo")
	private Set<Viaje> viajes = new HashSet<>();
	
	 
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	
	public Set<Viaje> getViajes() {
		return viajes;
	}
	public void setViajes(Set<Viaje> viajes) {
		this.viajes = viajes;
	}
	
	
	
}
