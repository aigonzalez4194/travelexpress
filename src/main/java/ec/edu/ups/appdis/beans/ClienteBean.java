package ec.edu.ups.appdis.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import ec.edu.ups.appdis.bussiness.ClienteBussiness;
import ec.edu.ups.appdis.dao.ClienteDao;
import ec.edu.ups.appdis.model.Cliente;


@Named("bean_cliente")
@ViewScoped
public class ClienteBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private ClienteDao daoCliente;
	
	@Inject
	private ClienteBussiness cliBussines;
	
	@Inject
	private FacesContext facesContext;
	
	private Cliente newCliente;
	private List<Cliente> cliente;
	private String mensaje;
	
	
	 private String firstname;
     private String lastname;
	/*
	 * Variables globales para validaciones
	 */
	private String cedula;
	private String mensajeErrorCedula;
	
	private String password;
    private String mensajeErrorPassword;
	
	@PostConstruct
	public void init() {
		newCliente=new Cliente();
		//cliente= daoCliente.getCliente();		
	}
	
	public void limpiarFormulario () {
		newCliente.setUsuario(" ");
		newCliente.setApellidos("");
		newCliente.setNombres("");
		newCliente.setClave("");
		newCliente.setDirecion("");
		newCliente.setEmail("");
		setCedula("");
	}
	
	public String guardarCliente() throws Exception {	
		try {
				newCliente.setCedula(cedula);
				newCliente.setClave(password);
				cliBussines.save(newCliente);
				System.out.println("Registro exitoso, nuevo cliente insertado");
//				FacesMessage msg = new FacesMessage("Aviso","Nuevo cliente registrado");
//		        FacesContext.getCurrentInstance().addMessage(null, msg);		
		        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Su cuenta de usuario fue creado satisfactoriamente"));
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("No se puede crear el usuario");
			FacesMessage msg = new FacesMessage("Aviso","Error de registro");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}finally {
			limpiarFormulario();
		}
		return "";
	}
	
public String ListarClientes() {
		try {
			cliente = cliBussines.getListadoClientes();
			System.out.println("Datos encontrados"+" " + getCliente().size());		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			 
	            FacesMessage m = new FacesMessage(
	            		FacesMessage.SEVERITY_INFO, "Error!", e.getMessage());
	            facesContext.addMessage(null, m);
			e.printStackTrace();
		}		
		return null;	
	}
	
	public String Eliminar (int cod )throws Exception {
		try {
			System.out.println("Entro metodo eliminar");
			//daoCliente.remove(cod);
			cliBussines.eliminar(cod);
			init();
			FacesMessage msg = new FacesMessage("Aviso","Un Cliente ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
	        //FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Cliente eliminado "+newCliente.getMensaje()));
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Error","El registro no ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}		
		return null;
	}
	
	
	/**
	 * Actualizar datos a nivel de fila (Cliente)
	 * @param event
	 * @throws Exception 
	 */
	 public void onRowEdit(RowEditEvent event) throws Exception {
		 Cliente cliente = (Cliente) event.getObject();
		 cliBussines.actualizar(cliente);
         FacesMessage msg = new FacesMessage("Cliente actualizado", ""+((Cliente) event.getObject()).getCedula());
         FacesContext.getCurrentInstance().addMessage(null, msg);
	    }
	 public void onRowCancel(RowEditEvent event) throws Exception {
	 	Cliente cli = (Cliente) event.getObject();
	 	cliBussines.actualizar(cli);
        FacesMessage msg = new FacesMessage("Actualizacion cancelada", ""+((Cliente) event.getObject()).getCedula());
        FacesContext.getCurrentInstance().addMessage(null, msg);
	    }

	 /**
	  * Metodo para validar cedula con mensajes de Primefaces
	  * @return
	  */
	 public String validaCedula() {
		 try {
			 System.out.println("Entro metodo validar cedula");
			 cliBussines.validaCedula(cedula);
			 if (cliBussines.getMensajeValidaCedula().equals("Ingrese su cedula de 10 digitos")) {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Su cedula: "+" "+cedula+", "+"debe tener 10 digitos"));
			}
			 if (cliBussines.getMensajeValidaCedula().equals("Su cedula es correcta")) {
			     FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Su cedula: "+" "+cedula+", "+"es correcta"));
			}
			 if (cliBussines.getMensajeValidaCedula().equals("Su cedula es incorrecta")) {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Su cedula: "+" "+cedula+", "+"es incorrecta"));
//				 FacesMessage msg = new FacesMessage("Error","Su cedula es incorrecta: "+cedula);
//			     FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "No se puede ejecutar el validador de cedula"));
		}
		 return null;
	 }
	 
	 /**
	  * Metodo para validar cedula y mostrar mensajes con AJAX 
	  * @param evento
	  */
	 @SuppressWarnings("unlikely-arg-type")
	public void validarCedula(AjaxBehaviorEvent evento) {
			 System.out.println("Entro metodo validar cedula");
			 cliBussines.validaCedula(cedula);
			 if (cedula.equals(cliBussines.getMensajeValidaCedula().equals("Ingrese su cedula de 10 digitos"))) {
				 mensajeErrorCedula="Ingrese su cedula de 10 digitos";
			}
			 else if (cedula.equals(cliBussines.getMensajeValidaCedula().equals("Su cedula es correcta"))) {
				 mensajeErrorCedula="Su cedula es correcta";
			}
			 else if (cedula.equals(cliBussines.getMensajeValidaCedula().equals("Su cedula es incorrecta"))) {
				 mensajeErrorCedula="Su cedula es correcta";
			}else {
				mensajeErrorCedula="";
			}
	 }
	 
	    /**
	     * Comprueba que la longitud de la password este en el rango correcto
	     * 
	     * @param evento 
	     */
	    public void validarPassword(AjaxBehaviorEvent evento) {
	        if (password.length() < 6) {
	            mensajeErrorPassword = "La contraseña tiene que tener como minimo 6 caracteres";
	        } else {
	            if (password.length() > 15) {
	                mensajeErrorPassword = "La contraseña puede tener como maximo 15 caracteres";
	            } else {
	                mensajeErrorPassword = "";
	            }
	        }
	    }
	 
	    
	    public void save() {
	        FacesContext.getCurrentInstance().addMessage(null,
	                new FacesMessage("Welcome " + firstname + " " + lastname));
	    }
	/*
	 * Generate Getters and Setters	 	
	 */
	public String Mensaje(String mensaje) {
		mensaje=this.mensaje;
		
		return null;
	}
	
	
	public Cliente getNewCliente() {
		return newCliente;
	}
	
	
	public void setNewCliente(Cliente newCliente) {
		this.newCliente = newCliente;
	}
		
	public List<Cliente> getCliente() {
		return cliente;
	}
	
	
	public void setCliente(List<Cliente> cliente) {
		this.cliente = cliente;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMensajeErrorPassword() {
		return mensajeErrorPassword;
	}

	public void setMensajeErrorPassword(String mensajeErrorPassword) {
		this.mensajeErrorPassword = mensajeErrorPassword;
	}

	public String getMensajeErrorCedula() {
		return mensajeErrorCedula;
	}

	public void setMensajeErrorCedula(String mensajeErrorCedula) {
		this.mensajeErrorCedula = mensajeErrorCedula;
	}

	
	
	
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
	
}
