package ec.edu.ups.appdis.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Cliente implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	@Column(length=15)
	@NotNull(message="Agregar usuario")
	private String usuario;
	
	@Column(length=15)
	@NotNull(message="Agregar clave")
	private String clave;
	
	@Column(length=10)
	@NotNull(message="Agregar cedula")
	private String cedula;
	
	@Column(length=100)
	@NotNull(message="Agregar nombres")
	private String nombres;
	
	@Column(length=100)
	@NotNull(message="Agregar apellidos")
	private String apellidos;
	
	@Column(length=100)
	@NotNull(message="Agregar direccion")
	private String direcion;
	
	
	private String mensaje;
	
	
	@Email
	@NotNull
	@NotEmpty
	@Column(length=50,unique=true)
	private String email;
	
	
	@OneToMany(cascade= {CascadeType.ALL}, fetch=FetchType.EAGER)
	@JoinColumn(name="viaje_cliente_id", referencedColumnName="codigo")
	private Set<Viaje> viajes = new HashSet<>();
	
	
	@OneToMany(cascade= {CascadeType.MERGE}, fetch=FetchType.EAGER)
	@JoinColumn(name="evento_cliente_id", referencedColumnName="codigo")
	private Set<Evento> eventos = new HashSet<>();
	

	/*
	 * Generate Getters and Setters
	 */
	public int getCodigo() {
		return codigo;
	}


	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}


	public String getCedula() {
		return cedula;
	}


	public void setCedula(String cedula) {
		this.cedula = cedula;
	}


	public String getNombres() {
		return nombres;
	}


	public void setNombres(String nombres) {
		this.nombres = nombres;
	}


	public String getApellidos() {
		return apellidos;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}


	public String getDirecion() {
		return direcion;
	}


	public void setDirecion(String direcion) {
		this.direcion = direcion;
	}


	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getClave() {
		return clave;
	}


	public void setClave(String clave) {
		this.clave = clave;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Set<Viaje> getViajes() {
		return viajes;
	}


	public void setViajes(Set<Viaje> viajes) {
		this.viajes = viajes;
	}


	public Set<Evento> getEventos() {
		return eventos;
	}


	public void setEventos(Set<Evento> eventos) {
		this.eventos = eventos;
	}

//
//	public Rol getRol() {
//		return rol;
//	}
//
//
//	public void setRol(Rol rol) {
//		this.rol = rol;
//	}
	
	
	
}
