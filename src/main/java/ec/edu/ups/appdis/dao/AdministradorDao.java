package ec.edu.ups.appdis.dao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.model.Admin;

@Stateless
public class AdministradorDao {
	
	@Inject
	private EntityManager em;
	
	
	public Admin loginAdmin(String user, String password) {
		
		try {
			String sql = "SELECT a FROM Admin a WHERE a.usuario= :user AND a.clave= :password";
			Query q = em.createQuery(sql, Admin.class);
			q.setParameter("user", user);
			q.setParameter("password", password);
			Admin adm = (Admin) q.getSingleResult();
			if(adm!=null)
				return adm;
			System.out.println("admin obtiene: "+adm.getApellido());
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

}
