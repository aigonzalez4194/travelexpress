package ec.edu.ups.appdis.beans;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import org.primefaces.event.RowEditEvent;
import com.lowagie.text.pdf.codec.Base64;

import ec.edu.ups.appdis.bussiness.ClienteBussiness;
import ec.edu.ups.appdis.bussiness.PagoBussiness;
import ec.edu.ups.appdis.bussiness.RentaBussiness;
import ec.edu.ups.appdis.bussiness.TipoBussiness;
import ec.edu.ups.appdis.model.Auto;
import ec.edu.ups.appdis.model.Cliente;
import ec.edu.ups.appdis.model.Pago;
import ec.edu.ups.appdis.model.Renta;
import ec.edu.ups.appdis.model.Tipo_Pago;
import ec.edu.ups.appdis.service.hiloEnvioCorreo;



@Named("bean_renta")
//@ViewScoped
@ViewScoped
public class RentaBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private RentaBussiness rentaBussiness;
	
	@Inject 
	private PagoBussiness pagoBussiness;
	private Pago newPago;
	
	@Inject
	private FacesContext context;
	
	@Inject
	private Sesion sesion;
	
	@Inject
	private RentaBussiness rB;
//	private List<Renta> rentaCliente;
//	
//	
//	private String usu = "";

	/*
	 * Instancia del Bean Auto
	 */
	@SuppressWarnings("deprecation")
	@ManagedProperty(value="#{bean_auto}")
	private AutoBean autoBean;
	
	/*
	 * Instancia del Bean Pago
	 */
	@SuppressWarnings("deprecation")
	@ManagedProperty(value="#{bean_pago}")
	private PagoBean pagoBean;
	
		
	private Renta newRenta;
	private Renta selectRenta;
	private List<Renta> renta;
	private Date fecha;
	private List<Renta> historialRentaUsuario;
	//private String usuarioHistorial;
	//private int numeroDiasRenta;
	
	/*
	 * Variables para cargar el tipo de pago al Combo, Pago
	 */
	private List<Tipo_Pago> listaTipoPago;
	private List<SelectItem> listadoTipoPagoCombo;
	private String selecionTipoPago;
	
	@Inject
	private TipoBussiness tpagoBussiness;
	
	@Inject
	private ClienteBussiness cliB;
	
	 /**
	 * listas y campos para manejar los combos
	 */
	private List<SelectItem> listaHoraRenta;
	private String lhr;
	
	private List<SelectItem> listaMinutosRenta;
	private String lmr;
	
	private int codigoActualizarAuto; //Parametro para actualizar Auto
	
	
	/**
	 * Para imagen del auto
	 */
	private Part file;
	
	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}

	@PostConstruct
	public void init() {
		
		newRenta=new Renta();
		newPago = new Pago();
		renta = rentaBussiness.getListadoRentas();	

		
		cargarHoraRenta();
		cargarMinutos();
		
		cargarSeleccionTipoPago();
	}


	/**
	 * Metodo para convertir Imagen de bytes a Base64 y poder visualizar en la Web.
	 * @param au
	 * @return
	 */
	public String toBase64ImgAuto (Auto au) {
		try {
			byte [] a = au.getImagen_auto();
			return "data:image/jpg;base64,"+Base64.encodeBytes(a);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
    public String historial() {
    	
    	
    	historialRentaUsuario = rentaBussiness.obtenerHistorial(sesion.getCliente().getUsuario());
    	
    	System.out.println("Tamanio total de rentas usuairo: "+historialRentaUsuario.size());
    	return null;
    }
	public String cancelarActualizacion() {
		return "viewAuto.xhtml?faces-redirect=true";
	}
	
	/**
	 * Metodo para cargar (tipoPago) a la lista del combo.
	 */
	public void cargarSeleccionTipoPago (){
		listadoTipoPagoCombo = new ArrayList<SelectItem>();
		listaTipoPago=tpagoBussiness.getListadoTPago();
		for(int i=0; i<listaTipoPago.size(); i++){
			listadoTipoPagoCombo.add(new SelectItem(listaTipoPago.get(i).getCodigo(),listaTipoPago.get(i).getDescripcion()));
//			System.out.println("--------Carga categorias opcion--------");
		}
	}
	
	
	public void seleccionarTipoPagoCombo(){
		try {
			for (int i=0; i<listaTipoPago.size(); i++){
				if((""+listaTipoPago.get(i).getCodigo()).equals(selecionTipoPago)){
					newPago.setTpago(listaTipoPago.get(i));

					System.out.println("Codigo tipoPago: "+ listaTipoPago.get(i).getCodigo()+ " ==> "+ "Nombre: "+listaTipoPago.get(i).getDescripcion());
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
//	public String guardarPago() {
//		
//		try {
//			pagoBussiness.save(newPago);
//			
//			System.out.println("Registro satisfactorio");
//			
//			return "";
//		} catch (Exception e) {
//			System.out.println("Error al guardar pago");
//		}
//		return null;
//	}
	
	 /**
	  * Metodo para redondear Valor a Pagar
	  * @param numero
	  * @return
	  */
	public  double redondear(double numero){
       // return Math.rint(numero*100)/100;
       return Math.round(numero*100.0)/100.0;
    }
	
	/**
	 * Metodo para guardar una renta, realizada por el Usuario
	 * @return
	 */
	public String guardar(){	
		try {
//			newPago.setValorTotal(redondear(sesion.getValorTotal()));
			pagoBussiness.guardar(newPago);
			//Integer.parseInt(autoBean.getUsuarioSession()) ;
			System.out.println("///////// ///////"+ sesion.getAuto().getModelo());
			
			System.out.println("///////////////// Dato del usuario /// //////////////"+sesion.getCliente().getNombres());
			
			System.out.println("Valor total a pagar RENTA: "+ sesion.getValorTotal()+"..............");
			
			/*
			 * Date Format
			 */
			
			newRenta.setHora(sesion.getHoraGlobal());
			newRenta.setCliente(sesion.getCliente());
			newRenta.setFecha(sesion.getFechaGlobal());		
			newRenta.setNumeroDias(sesion.getCantDiasRenta());
			newRenta.setAuto(sesion.getAuto());
			newRenta.setEstado("Rentado");
//			
			newRenta.setPago(newPago);
//			
			
			
			/*
			 * Instanciamos el hilo para enviar el correo luego de guardar la Renta (Backgroud - Segundo plano)
			 */
			hiloEnvioCorreo hilo = new hiloEnvioCorreo();
			hilo.enviarRenta(sesion.getCliente().getEmail(), 
							 sesion.getAuto().getModelo(), 
							 sesion.getCantDiasRenta(), 
							 sesion.getValorTotal(),
							 sesion.getFechaGlobal(),
							 sesion.getHoraGlobal(),
							 sesion.getCliente().getNombres());
			
			rentaBussiness.save(newRenta); //Finalmente guardamos la renta
//			
//			
	        FacesMessage msg = new FacesMessage("Aviso","Su renta ha sido registrado satisfactoriamente");
	        context.addMessage(null, msg);
	        //FacesContext.getCurrentInstance().addMessage(null, msg);
			System.out.println("Registro Guardado");
					
		} catch (Exception e) {
			System.out.println("Error al guardar"); 
			 FacesMessage msg = new FacesMessage("Error","No se puede realizar la renta");
	         //FacesContext.getCurrentInstance().addMessage(null, msg);
	         context.addMessage(null, msg);
			e.printStackTrace();
		}	
		return "Renta.xhtml?faces-redirect=true";	
	}

	public String pagarRenta() {
		
		return "pagoRenta.xhtml?faces-redirect=true";
	}
	
	public String eliminar(int cod) {
		try {
			rentaBussiness.eliminar(cod);
			init();
			FacesMessage msg = new FacesMessage("Aviso","Renta eliminada"+cod);
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Error","El registro no ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		return null;
	}
	

	
	/**
	 * Metodo para listar los autos registrados
	 * @return
	 */
	public String ListarAutos() {
		try {
			renta=rentaBussiness.getListadoRentas();
			System.out.println("Datos encontrados"+" " + getRenta().size());		
		} catch (Exception e) {
			System.out.println("Error al listar datos");
            FacesMessage m = new FacesMessage("Error!", "No se puede obtener los datos");
            FacesContext.getCurrentInstance().addMessage(null, m);
			e.printStackTrace();
		}		
		return null;	
	}
	
	private void cargarHoraRenta(){
		listaHoraRenta = new ArrayList<SelectItem>();
		listaHoraRenta	 .add(new SelectItem("Seleccione"));
		listaHoraRenta   .add(new SelectItem("00"));
		listaHoraRenta   .add(new SelectItem("01"));
		listaHoraRenta   .add(new SelectItem("02"));
		listaHoraRenta   .add(new SelectItem("03"));
		listaHoraRenta   .add(new SelectItem("04"));
		listaHoraRenta   .add(new SelectItem("05"));
		listaHoraRenta   .add(new SelectItem("06"));
		listaHoraRenta   .add(new SelectItem("07"));
		listaHoraRenta   .add(new SelectItem("08"));
		listaHoraRenta   .add(new SelectItem("09"));
		listaHoraRenta   .add(new SelectItem("10"));
		listaHoraRenta   .add(new SelectItem("11"));
		listaHoraRenta   .add(new SelectItem("12"));
		listaHoraRenta   .add(new SelectItem("13"));
		listaHoraRenta   .add(new SelectItem("14"));
		listaHoraRenta   .add(new SelectItem("15"));
		listaHoraRenta   .add(new SelectItem("16"));
		listaHoraRenta   .add(new SelectItem("17"));
		listaHoraRenta   .add(new SelectItem("18"));
		listaHoraRenta   .add(new SelectItem("19"));
		listaHoraRenta   .add(new SelectItem("20"));
		listaHoraRenta   .add(new SelectItem("21"));
		listaHoraRenta   .add(new SelectItem("22"));
		listaHoraRenta   .add(new SelectItem("23"));

	}
	private void cargarMinutos() {
		listaMinutosRenta = new ArrayList<SelectItem>();
		listaMinutosRenta .add(new SelectItem("Seleccione"));
		listaMinutosRenta.add(new SelectItem("00"));
		listaMinutosRenta.add(new SelectItem("15"));
		listaMinutosRenta.add(new SelectItem("30"));
		listaMinutosRenta.add(new SelectItem("45"));
	}
	
	
	/**
	 * Metodo para setear la hora selecionada del ComboBox
	 */	
	public String seleccionarHora() {
		try {
			if(!lhr.equals("") && !lmr.equals("")) {
				//newRenta.setHora(lhr +":"+ lmr);
				System.out.println("La hora seleccionada es:"+ lhr +" "+ "Minuto: "+lmr);
				String hora= lhr+ ":" + lmr;
				sesion.setHoraGlobal(hora); //Seteo la hora para recuperar en la seleccion del Auto
				return hora;
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}
//	public String consultaHistorial() {
//			historialUsuario=rentaBussiness.obtenerHistorial(usuarioHistorial);
//		return null;
//	}
//	
	/**
	 * Actualizar datos a nivel de fila (Auto)
	 * @param event
	 * @throws Exception 
	 */
//	 public void onRowEdit(RowEditEvent event) throws Exception {
//		 Re a = (Auto) event.getObject();
//		 auBussiness.actualizar(a);
//         FacesMessage msg = new FacesMessage("Auto actualizado", ""+((Auto) event.getObject()).getModelo());
//         FacesContext.getCurrentInstance().addMessage(null, msg);
//	    }
//	 public void onRowCancel(RowEditEvent event) throws Exception {
//		 Auto a = (Auto) event.getObject();
//	 	 auBussiness.actualizar(a);
//         FacesMessage msg = new FacesMessage("Actualizacion cancelada", ""+((Auto) event.getObject()).getModelo());
//         FacesContext.getCurrentInstance().addMessage(null, msg);
//	    }

	public Renta getNewRenta() {
		return newRenta;
	}

	public void setNewRenta(Renta newRenta) {
		this.newRenta = newRenta;
	}

	public List<Renta> getRenta() {
		return renta;
	}

	public void setRenta(List<Renta> renta) {
		this.renta = renta;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public List<SelectItem> getListaHoraRenta() {
		return listaHoraRenta;
	}

	public void setListaHoraRenta(List<SelectItem> listaHoraRenta) {
		this.listaHoraRenta = listaHoraRenta;
	}

	public String getLhr() {
		return lhr;
	}

	public void setLhr(String lhr) {
		this.lhr = lhr;
	}

	public List<SelectItem> getListaMinutosRenta() {
		return listaMinutosRenta;
	}

	public void setListaMinutosRenta(List<SelectItem> listaMinutosRenta) {
		this.listaMinutosRenta = listaMinutosRenta;
	}

	public String getLmr() {
		return lmr;
	}

	public void setLmr(String lmr) {
		this.lmr = lmr;
	}

	public Renta getSelectRenta() {
		return selectRenta;
	}

	public void setSelectRenta(Renta selectRenta) {
		this.selectRenta = selectRenta;
	}

	public Pago getNewPago() {
		return newPago;
	}

	public void setNewPago(Pago newPago) {
		this.newPago = newPago;
	}

	public List<Tipo_Pago> getListaTipoPago() {
		return listaTipoPago;
	}

	public void setListaTipoPago(List<Tipo_Pago> listaTipoPago) {
		this.listaTipoPago = listaTipoPago;
	}

	public List<SelectItem> getListadoTipoPagoCombo() {
		return listadoTipoPagoCombo;
	}

	public void setListadoTipoPagoCombo(List<SelectItem> listadoTipoPagoCombo) {
		this.listadoTipoPagoCombo = listadoTipoPagoCombo;
	}

	public String getSelecionTipoPago() {
		return selecionTipoPago;
	}

	public void setSelecionTipoPago(String selecionTipoPago) {
		this.selecionTipoPago = selecionTipoPago;
	}
   
	
	
//	public String getUsu() {
//		return usu;
//	}
//
//	public void setUsu(String usu) {
//		this.usu = usu;
//	}

//	public List<Renta> getHistorialUsuario() {
//		return historialUsuario;
//	}
//
//	public void setHistorialUsuario(List<Renta> historialUsuario) {
//		this.historialUsuario = historialUsuario;
//	}
	
	
}
