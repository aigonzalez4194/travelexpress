package ec.edu.ups.appdis.beans;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import ec.edu.ups.appdis.dao.AdministradorDao;
import ec.edu.ups.appdis.dao.ChoferDao;
import ec.edu.ups.appdis.dao.ClienteDao;
import ec.edu.ups.appdis.model.Admin;
import ec.edu.ups.appdis.model.Chofer;
import ec.edu.ups.appdis.model.Cliente;
import ec.edu.ups.appdis.util.Util;

//@ManagedBean(name="bean_login")
@Named("bean_login")
@SessionScoped
public class LoginBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	/**
	 * Parametros para comparar datos del Usuario
	 */
	private String user;
	private String password;
	private List<Cliente> listUser;
	private Cliente usuario; //Instancia de objeto Cliente
	
	private String tipoUsuario;
	
	private Cliente cli;
	
	@Inject
	private ClienteDao daoCliente;
	
	@Inject
	private AdministradorDao daoAdmin;
	
	@Inject 
	private ChoferDao daoChofer;
	private List<Chofer> choferes;
	
	
	@Inject
	private Sesion sesion;
	
	@Inject
	private AutoBean auto;
	/*
	 * Instancia del Bean Pago
	 */
//	@SuppressWarnings("deprecation")
//	@ManagedProperty(value="#{bean_pago}")
//	private PagoBean pagoBean;

	@PostConstruct
	public void init(){
		//usuario = new Cliente();
		choferes = daoChofer.getChofer(); //Obtenemos la lista de choferes
		
		System.out.println("Choferes: "+ choferes.size());
	}
	
	public String loginUser() {
		System.out.println("Entrooooooooooooooooooooooooooooooooooo metodo login user ");
//		try {
			usuario = daoCliente.loginUser(user, password);
			
			Admin adm = daoAdmin.loginAdmin(user, password);		
			
		
//			System.out.println(adm.getApellido()+" . .. .");
			String userObtenido = "";
			String pwdObtenido ="" ;
					
			if (adm!=null) {
//				System.out.println(adm.getApellido()+" . .. .2222");
				userObtenido = adm.getUsuario();
				pwdObtenido = adm.getClave();
				
				tipoUsuario="administrador";
			}

			if(usuario!=null){
				
				userObtenido = usuario.getUsuario();
				pwdObtenido = usuario.getClave();					
				tipoUsuario="cliente";														
			}
			System.out.println(userObtenido+"Usuario cliente..........................");
			if (user.equals(userObtenido)&&password.equals(pwdObtenido)) {
				
				if(tipoUsuario.equals("administrador")) {
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", adm);
					return"/Cliente.xhtml?faces-redirect=true";
				}else {
					FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario",  usuario);
					sesion.setCliente(usuario); //Seteo los datos del Usuario para hacer refencia al momento de Guardar Renta, Viaje y Evento
					System.out.println("Usuario obtenido para guardar: "+sesion.getCliente().getNombres());
					sesion.setChofer(choferes.get(0));
					return"/Renta.xhtml?faces-redirect=true";
				}
			}		
			else{	
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error!","Usuario o contraseña no son correctos. Por favor vuelva a intentar"));
				System.out.println("Datos de usuario erroneos");		
				//this.setValideUser("Usuario o clave incorrecta...!");
				return null;		
			}
//		} catch (Exception e) {
//			// TODO: handle exception
//						String errorMessage = getRootErrorMessage(e);
//			            FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, "login unsuccessful");
//			            facesContext.addMessage(null, m);      
//		}
//		
//		return "#";
	}
	
	public Cliente listaUsurios (){
		
		return listUser.get(0);
	}
	

		
	public String logout () {
		HttpSession session = Util.getSession();
		session.invalidate();
		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Aviso","Sesion finalizada satisfactoriamente"));
		return "inicio?faces-redirect=true";
	}
	
	
	
	/*
	 * Setters and Getters
	 */
	
	public Sesion getSesion() {
		return sesion;
	}
	public void setSesion(Sesion sesion) {
		this.sesion = sesion;
	}
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Cliente> getListUser() {
		return listUser;
	}

	public void setListUser(List<Cliente> listUser) {
		this.listUser = listUser;
	}
	public Cliente getUsuario() {
		return usuario;
	}
	public void setUsuario(Cliente usuario) {
		this.usuario = usuario;
	}
	
	
}
