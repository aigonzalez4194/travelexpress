package ec.edu.ups.appdis.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Viaje {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo_viaje;
	
	@NotNull
	private Date fecha;
	
	@Column(length=20)
	@NotNull(message="Seleccionar hora")
	private String hora;
	
	@NotNull(message="Seleccionar asiento")
	private String asientos_disp;
	
	
	@ManyToOne
	@JoinColumn(name="viaje_auto_id", nullable=false)
	private Auto auto;
	
	/*
	 * Relacion de Muchos a uno. Varios viajes se pueden realizar a un mismo luegar
	 */
	@ManyToOne
	@JoinColumn(name="lugar_viaje_id", nullable=false)
	@JsonIgnore
	private Lugar lugar; 
	
	@ManyToOne
	@JoinColumn(name="viaje_chofer_id", nullable=false)
	@JsonIgnore
	private Chofer chofer; 
	
	@ManyToOne
	@JoinColumn(name="viaje_cliente_id", nullable=false)
	@JsonIgnore
	private Cliente cliente; 
	
	@ManyToOne
	@JoinColumn(name="viaje_pago_id", nullable=false)
	@JsonIgnore
	private Pago pago; 
	
	
	
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getAsientos_disp() {
		return asientos_disp;
	}
	public void setAsientos_disp(String asientos_disp) {
		this.asientos_disp = asientos_disp;
	}
	public int getCodigo_viaje() {
		return codigo_viaje;
	}
	public void setCodigo_viaje(int codigo_viaje) {
		this.codigo_viaje = codigo_viaje;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public Lugar getLugar() {
		return lugar;
	}
	public void setLugar(Lugar lugar) {
		this.lugar = lugar;
	}
	public Chofer getChofer() {
		return chofer;
	}
	public void setChofer(Chofer chofer) {
		this.chofer = chofer;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public Auto getAuto() {
		return auto;
	}
	public void setAuto(Auto auto) {
		this.auto = auto;
	}
	public Pago getPago() {
		return pago;
	}
	public void setPago(Pago pago) {
		this.pago = pago;
	}	
	
}
