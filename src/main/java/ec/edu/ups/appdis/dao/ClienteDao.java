package ec.edu.ups.appdis.dao;


import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import ec.edu.ups.appdis.model.Cliente;
import ec.edu.ups.appdis.model.Renta;

@Stateless
public class ClienteDao {
	
	@Inject
	private EntityManager em;
	
	public void insert(Cliente cliente ) {	
		em.persist(cliente);	
		}
	
	public void update(Cliente cliente){
		em.merge(cliente);
		
	}
	public void remove(int id){
		em.remove(read(id));
		
	}
	public Cliente read(int id){
		Cliente aux=em.find(Cliente.class, id);
		return aux;
		  
	}
	
	/**
	 * Metodo para listar Clientes Registrados
	 * @return
	 */
	public List<Cliente>getCliente(){
		
		String jpaql="Select cl from Cliente cl";	
		Query query=em.createQuery(jpaql,Cliente.class);
		List<Cliente>listar=query.getResultList();
		return listar;
	}
	
	/**
	 * Metodo para listar Clientes Registrados
	 * @return
	 */
	public List<Cliente>getUsuario( String username){
		System.out.println("Consulta para recuperar usuario de App Movil");
		String jpaql="Select cl from Cliente cl WHERE cl.usuario= :username";	
		Query query=em.createQuery(jpaql,Cliente.class);
		query.setParameter(username, username);
		List<Cliente>listar=query.getResultList();
		return listar;
	}
	
	/**
	 * Metodo para consultar usuarios a la BD para el inicio de sesion.
	 * @return
	 */
	public Cliente loginUser( String user, String password){
		
		try {
			System.out.println("Consulta de inicio de sesion");
			String jpaql="SELECT u FROM Cliente u WHERE u.usuario= :user and u.clave = :password";	
			Query query=em.createQuery(jpaql, Cliente.class);
			query.setParameter("user", user);
		    query.setParameter("password", password);
			Cliente cli= (Cliente) query.getSingleResult();
			
			if(cli!=null)
				return cli;
					System.out.println("Usuario obtenido: "+cli);
			return null;
		} catch (Exception e) {
			// TODO: handle exception
		}
		
			return null;

	}
	
//	public Cliente  loginUser( String user, String password){
//		try {
//			System.out.println("Consulta de inicio de sesion");
//			String jpaql="SELECT u FROM Cliente u left JOIN FETCH u.Rol r WHERE u.usuario= :user and u.clave = :password";	
//			Query query=em.createQuery(jpaql, Cliente.class);
//			query.setParameter("user", user);
//		    query.setParameter("password", password);
//			Cliente cli =(Cliente) query.getSingleResult();
//			System.out.println("Datos de consulta:"+" "+cli);
//			
//			if(cli!=null)
//				
//				return cli;
//			
//		} catch (Exception e) {
//			e.printStackTrace();			
//		}
//		return null;
//				
//	}

	/*
	 * metodo de buscar  persona para logueo
	 */
//	public Cliente buscarUser(String usuario, String pass) {
//		System.out.println("Entro metodo consulta usuario ROl");
//		
//		try {
//			System.out.println("Intenta hacer consulta");
////			String jpaql = "SELECT u FROM Cliente u left JOIN  FETCH u.Rol  r WHERE u.usuario = :usuario and u.clave = :pass";
//			String jpaql = "SELECT u FROM Cliente u  WHERE u.usuario = :usuario and u.clave = :pass";
//			Query  query = em.createQuery(jpaql);
//			query.setParameter("usuario", usuario);
//			query.setParameter("pass", pass);
//			
//			Cliente p= (Cliente) query.getSingleResult();
//			System.out.println("Datos de consulta ROL: "+p);
//			
//			if (p!= null)
//				System.out.println("Datos de consulta ROL: "+p);
//				return p;
//			
//		}
//		catch (Exception e){}
//		
//		return null;
//	}
//	

}
