package ec.edu.ups.appdis.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.RowEditEvent;

import ec.edu.ups.appdis.bussiness.LugarBussiness;
import ec.edu.ups.appdis.bussiness.ViajeBussiness;
import ec.edu.ups.appdis.model.Lugar;
import ec.edu.ups.appdis.model.Tipo_Pago;
import ec.edu.ups.appdis.model.Viaje;


@Named("bean_viaje")
@ViewScoped
public class ViajeBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private ViajeBussiness vjBussiness;
	
	@Inject
	private LugarBussiness lugarBussiness;
	
	@Inject
	private FacesContext facesContext;
	
	private Viaje newViaje;
	private List<Viaje> viajes;
	private Date fechaViaje; //Parametro de fecha, que seleciona el usuario en la vista
	
	
	/*
	 * Variables para cargar el tipo de pago al Combo, Pago
	 */
	private List<Lugar> listaLugar;
	private List<SelectItem> listadoLugarCombo;
	private String selecionLugar;
	
	 /**
	 * listas y campos para manejar los combos
	 */
	private List<SelectItem> listaHoraViaje;
	private String lhv;
	
	@PostConstruct
	public void init() {
		
		newViaje=new Viaje();
		//viajes= daoViaje.getViajes();
		
		listaHoraViaje= new ArrayList<SelectItem>();
		cargarHoraViaje();
		cargarSeleccionLugar();
			
	}
	
	
	
	
	/**
	 * Metodo para cargar (Lugares) a la lista del combo.
	 */
	public void cargarSeleccionLugar (){
		listadoLugarCombo = new ArrayList<SelectItem>();
		listaLugar=lugarBussiness.getListadoLugares();
		for(int i=0; i<listaLugar.size(); i++){
			listadoLugarCombo.add(new SelectItem(listaLugar.get(i).getCodigo(),listaLugar.get(i).getNombre()));
//			System.out.println("--------Carga categorias opcion--------");
		}
	}
	
	
	public void seleccionarLugarCombo(){
		try {
			for (int i=0; i<listaLugar.size(); i++){
				if((""+listaLugar.get(i).getCodigo()).equals(selecionLugar)){
					newViaje.setLugar(listaLugar.get(i)); //Seteamos el lugar obtenido a la Entidad Viaje
					System.out.println("Codigo Lugar: "+ listaLugar.get(i).getCodigo()+ " ==> "+ "Descripcion: "+listaLugar.get(i).getNombre());
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Metodo para guardar el viaje en BD
	 * @return
	 */
	public String guardar() {	
		try {
			newViaje.setFecha(fechaViaje);//Setea la fecha escogida por el usuario (Primefaces)
			vjBussiness.save(newViaje);
			System.out.println("Registro Guardado");
			FacesMessage msg = new FacesMessage("Aviso","Nuevo viaje registrado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("No se puede insertar el registro");
			FacesMessage msg = new FacesMessage("Error","Error de registro");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}	
		return null;	
	}

	
	public String eliminar (int cod) {
		try {
			vjBussiness.eliminar(cod);
			FacesMessage msg = new FacesMessage("Aviso","Un elemento ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Error","El registro no ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		return null;
	}
	
	/**
	 * Metodo para listar los  viajes registrados
	 * @return
	 */
	public String ListarViajes() {
		try {
			vjBussiness.getListadoViajes();
			System.out.println("Datos encontrados"+" " + getViajes().size());		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			 
	            FacesMessage m = new FacesMessage(
	            		FacesMessage.SEVERITY_INFO, "Error!", e.getMessage());
	            facesContext.addMessage(null, m);
			e.printStackTrace();
		}		
		return null;	
	}
	
	/**
	 * Actualizar datos a nivel de fila (Viaje)
	 * @param event
	 * @throws Exception 
	 */
	 public void onRowEdit(RowEditEvent event) throws Exception {
		 Viaje l = (Viaje) event.getObject();
		 vjBussiness.actualizar(l);
         FacesMessage msg = new FacesMessage("Chofer actualizado", ""+((Viaje) event.getObject()).getCodigo_viaje());
         FacesContext.getCurrentInstance().addMessage(null, msg);
	    }
	 public void onRowCancel(RowEditEvent event) throws Exception {
		Viaje cli = (Viaje) event.getObject();
	 	vjBussiness.actualizar(cli);
        FacesMessage msg = new FacesMessage("Actualizacion cancelada", ""+((Viaje) event.getObject()).getCodigo_viaje());
        FacesContext.getCurrentInstance().addMessage(null, msg);
	    }
	 
		/**
		 * Metodo que permite agregar la hora de Salida que se vera en el comboBox
		 */
		private void cargarHoraViaje(){
			listaHoraViaje = new ArrayList<SelectItem>();
			listaHoraViaje  .add(new SelectItem("08:00"));
			listaHoraViaje  .add(new SelectItem("10:00"));
			listaHoraViaje  .add(new SelectItem("13:00"));
			listaHoraViaje  .add(new SelectItem("15:00"));
			listaHoraViaje  .add(new SelectItem("18:00"));
			listaHoraViaje  .add(new SelectItem("20:00"));
			listaHoraViaje  .add(new SelectItem("23:00"));

		}
		
		/**
		 * Metodo para setear la hora selecionada del ComboBox
		 */
		public void seleccionarHora() {
			if(!lhv.equals("")) {
				newViaje.setHora(lhv);
				System.out.println("La hora seleccionada es:"+"  "+lhv);
			}
		}
	 
	public Viaje getNewViaje() {
		return newViaje;
	}

	public void setNewViaje(Viaje newViaje) {
		this.newViaje = newViaje;
	}

	public List<Viaje> getViajes() {
		return viajes;
	}

	public void setViajes(List<Viaje> viajes) {
		this.viajes = viajes;
	}

	public String getLhv() {
		return lhv;
	}

	public void setLhv(String lhv) {
		this.lhv = lhv;
	}

	public List<SelectItem> getListaHoraViaje() {
		return listaHoraViaje;
	}

	public void setListaHoraViaje(List<SelectItem> listaHoraViaje) {
		this.listaHoraViaje = listaHoraViaje;
	}

	public Date getFechaViaje() {
		return fechaViaje;
	}

	public void setFechaViaje(Date fechaViaje) {
		this.fechaViaje = fechaViaje;
	}

	public List<Lugar> getListaLugar() {
		return listaLugar;
	}

	public void setListaLugar(List<Lugar> listaLugar) {
		this.listaLugar = listaLugar;
	}

	public List<SelectItem> getListadoLugarCombo() {
		return listadoLugarCombo;
	}

	public void setListadoLugarCombo(List<SelectItem> listadoLugarCombo) {
		this.listadoLugarCombo = listadoLugarCombo;
	}

	public String getSelecionLugar() {
		return selecionLugar;
	}

	public void setSelecionLugar(String selecionLugar) {
		this.selecionLugar = selecionLugar;
	}
	
}
