package ec.edu.ups.appdis.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class rentaApp {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	private String usuario;
	
	private String modelo;
	
	private String placa;
	
	private String fechaRenta;
	
	private String hora;
	
	private String numDias;
	
	private String formaPago;
	
	private String Titular;
	
	private String numeroT;
	
	private String anio;
	
	private String codigoSeguridad;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getFechaRenta() {
		return fechaRenta;
	}

	public void setFechaRenta(String fechaRenta) {
		this.fechaRenta = fechaRenta;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getNumDias() {
		return numDias;
	}

	public void setNumDias(String numDias) {
		this.numDias = numDias;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getTitular() {
		return Titular;
	}

	public void setTitular(String titular) {
		Titular = titular;
	}

	public String getNumeroT() {
		return numeroT;
	}

	public void setNumeroT(String numeroT) {
		this.numeroT = numeroT;
	}

	public String getAnio() {
		return anio;
	}

	public void setAnio(String anio) {
		this.anio = anio;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}
	
	
	
	
	
	
	
	
	
}
