package ec.edu.ups.appdis.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.PrimeFaces;
import org.primefaces.event.RowEditEvent;
import ec.edu.ups.appdis.bussiness.ChoferBussiness;
import ec.edu.ups.appdis.bussiness.ClienteBussiness;
import ec.edu.ups.appdis.model.Chofer;

@Named
@ViewScoped
public class ChoferBean implements Serializable {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject 
	private ChoferBussiness chofbussiness;
	
	@Inject
	private ClienteBussiness cliBussines;  
		
	private Chofer newPersona;
	private List<Chofer> chofer;
	
	private String cedula;
	
	@PostConstruct
	public void init() {
		
		newPersona=new Chofer();
		//chofer= daoChofer.getChofer();
		//persona=perBussiness.getCliente();
	
		
	}
	public List<Chofer> getChofer() {
		return chofer;
	}

	public void setChofer(List<Chofer> chofer) {
		this.chofer = chofer;
	}

	public Chofer getNewPersona() {
		return newPersona;
	}

	public void setNewPersona(Chofer newPersona) {
		this.newPersona = newPersona;
	}
	
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	
	public void limpiarFormulario() {
		newPersona.setApellidos("");
		newPersona.setNombres("");
		newPersona.setTelefono("");
		setCedula("");
	}
	
	public String guardar() throws Exception{
		FacesMessage message = null;
		try {
			newPersona.setCedula(cedula);
			chofbussiness.save(newPersona);
			System.out.println("Registro Guardado");            
            FacesMessage msg = new FacesMessage("Aviso","Nuevo chofer registrado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error al guardar");
            message=new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error", "No se pudo realizar el registro");
            PrimeFaces.current().dialog().showMessageDynamic(message);	
		}finally {
			limpiarFormulario();
		}
		return "";
		
	}
	
	public String eliminar (int cod) {	
		
		try {
			chofbussiness.eliminar(cod);
			init();
			FacesMessage msg = new FacesMessage("Aviso","Un chofer ha sido eliminado ");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Error","El registro no ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		return null;
	}
	
	/**
	 * Metodo para listar los choferes registrados
	 * @return
	 */
	public String ListarChoferes() {
		try {
			chofer=chofbussiness.getListadoChofer();
			System.out.println("Datos encontrados"+" " + getChofer().size());	
		} catch (Exception e) {
            System.out.println("No pudo recuperar datos");
			e.printStackTrace();
		}		
		return null;	
	}
	
	
	/**
	 * Actualizar datos a nivel de fila (Chofer)
	 * @param event
	 * @throws Exception 
	 */
	 public void onRowEdit(RowEditEvent event) throws Exception {
		Chofer c = (Chofer) event.getObject();
		chofbussiness.actualizar(c);
        FacesMessage msg = new FacesMessage("Chofer actualizado", ""+((Chofer) event.getObject()).getCedula());
        FacesContext.getCurrentInstance().addMessage(null, msg);
	    }
	 public void onRowCancel(RowEditEvent event) throws Exception {
	 	Chofer cli = (Chofer) event.getObject();
	 	chofbussiness.actualizar(cli);
        FacesMessage msg = new FacesMessage("Actualizacion cancelada", ""+((Chofer) event.getObject()).getCedula());
        FacesContext.getCurrentInstance().addMessage(null, msg);
	    }
	 
	 /**
	  * Metodo para validar cedula con mensajes de Primefaces
	  * @return
	  */
	 public String validaCedula() {
		 try {
			 System.out.println("Entro metodo validar cedula");
			 cliBussines.validaCedula(cedula);
			 if (cliBussines.getMensajeValidaCedula().equals("Ingrese su cedula de 10 digitos")) {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Su cedula: "+" "+cedula+", "+"debe tener 10 digitos"));
			}
			 if (cliBussines.getMensajeValidaCedula().equals("Su cedula es correcta")) {
			     FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Su cedula: "+" "+cedula+", "+"es correcta"));
			}
			 if (cliBussines.getMensajeValidaCedula().equals("Su cedula es incorrecta")) {
				 FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Su cedula: "+" "+cedula+", "+"es incorrecta"));
			}
			 
		} catch (Exception e) {
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "No se puede ejecutar el validador de cedula"));
		}
		 return null;
	 }

}
