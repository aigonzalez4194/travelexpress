package ec.edu.ups.appdis.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity

public class Admin {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(name="codigo", length=11)
	private int codigo;
	
	@Column(length=50)
	private String nombre;
	
	@Column(length=50)
	private String apellido;
	
	@Column(length=13)
	private String usuario;
	
	@Column(length=15)
	private String clave;
	
	
	
	/**
	 * Generated Getters and Setters
	 * @return
	 */
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getClave() {
		return clave;
	}
	public void setClave(String clave) {
		this.clave = clave;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	
		

}
