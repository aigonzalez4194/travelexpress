package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;


import ec.edu.ups.appdis.model.Lugar;

@Stateless
public class LugarDao {
	
	@Inject
	EntityManager em;
	
	public void insert(Lugar chofer ) {	
		em.persist(chofer);
		
		}
	
	public void update(Lugar chofer){
		em.merge(chofer);
		
	}
	public void remove(int id){
		em.remove(read(id));
		
	}
	public Lugar read(int id){
		Lugar aux=em.find(Lugar.class, id);
		return aux;
		  
	}
	
	public List<Lugar>getLugares(){
		
		String jpaql="Select p from Lugar p";	
		Query query=em.createQuery(jpaql,Lugar.class);
		List<Lugar>listar=query.getResultList();
		return listar;
	}
	

}
