package ec.edu.ups.appdis.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.Part;
import org.primefaces.event.RowEditEvent;
import com.lowagie.text.pdf.codec.Base64;
import ec.edu.ups.appdis.bussiness.AutoBussiness;
import ec.edu.ups.appdis.bussiness.RentaBussiness;
import ec.edu.ups.appdis.dao.AutoDao;
import ec.edu.ups.appdis.dao.RentaDao;
import ec.edu.ups.appdis.model.Auto;
import ec.edu.ups.appdis.model.Cliente;
import ec.edu.ups.appdis.model.Renta;



@Named("bean_auto")
@ViewScoped
//@SessionScoped
public class AutoBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject 
	private AutoBussiness auBussiness;

	@Inject
	private Sesion sesion;
	
	
	private Auto newAuto;
	private List<Auto> auto;
	
	private Auto selectAuto;
	private Map<Long, Boolean> selectedIds = new HashMap<Long, Boolean>();
    private List<Auto> selectedDataList;

	private Renta newRenta;
	private Date fechaRenta;
	private int codigoActualizarAuto; //Parametro para actualizar Auto
	
//	private Cliente usuarioSession;
	
	/*
	 * Parametros de confirmacion RENTA
	 */
//	private int codAutoSeleccionado; //Parametro para reservar renta del Auto
//	private String modelo;
	private int numAsientos;
//	private double precioAuto;
//	private String horaRenta;
	private int numeroDiasRenta;
	private double valorTotalRentaPorDias;
	
	public int getCodigoActualizarAuto() {
		return codigoActualizarAuto;
	}

	public void setCodigoActualizarAuto(int codigoActualizarAuto) {
		this.codigoActualizarAuto = codigoActualizarAuto;
		readActualizarAuto(codigoActualizarAuto); //Setea el codigo del auto seleccionado para actualizar sus datos
	}

	/**
	 * Para imagen del auto
	 */
	private Part file;
	
	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}
	
	@SuppressWarnings("deprecation")
	@ManagedProperty(value="#{bean_renta}")
	private RentaBean rentaBean;
	
	@PostConstruct
	public void init() {
		
		newAuto=new Auto();
		newRenta = new Renta();
		auto = auBussiness.getListadoAutos();
		
//		usuarioSession = login.listaUsurios();
//		System.out.println("Obtine usuuuuuuuuuuuuuuuuuuuuuario auto: "+usuarioSession.getCodigo());
	}

	/**
	 * Metodo para convertir Imagen de bytes a Base64 y poder visualizar en la Web.
	 * @param au
	 * @return
	 */
	public String toBase64ImgAuto (Auto au) {
		try {
			byte [] a = au.getImagen_auto();
			return "data:image/jpg;base64,"+Base64.encodeBytes(a);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public String readActualizarAuto(int codigo) {
		System.out.println("Cargando datos para actualizar: "+codigo);
		newAuto = auBussiness.readAuto(codigo);
		return null;
	}
	
	public String cancelarActualizacion() {
		return "viewAuto.xhtml?faces-redirect=true";
	}
	/**
	 * Metodo para guardar un auto
	 * @return
	 */
	public String guardar(){	
		try {
			//Conversion para guardar imagen del auto
			try {
				byte [] b = new byte [(int) file.getSize()];
					file.getInputStream().read(b);
					newAuto.setImagen_auto(b);
				} catch (Exception e) {
					e.printStackTrace();
				}
			
			
			auBussiness.save(newAuto);
	        FacesMessage msg = new FacesMessage("Aviso","Nuevo auto registrado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
			System.out.println("Registro Guardado");
			
		} catch (Exception e) {
			System.out.println("Error al guardar");
			 
			 FacesMessage msg = new FacesMessage("Error","Error de registro");
	         FacesContext.getCurrentInstance().addMessage(null, msg);
			e.printStackTrace();
		}	
		return "viewAuto.xhtml?faces-redirect=true";	
	}
	
	public String eliminar(int cod) {
		try {
			auBussiness.eliminar(cod);
			init();
			FacesMessage msg = new FacesMessage("Aviso","Registro eliminado"+cod);
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Error","El registro no ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}
		return null;
	}
	
	/**
	 * Metodo para listar los autos registrados
	 * @return
	 */
	public String ListarAutos() {
		try {
			auto=auBussiness.getListadoAutos();
			System.out.println("Datos encontrados"+" " + getAuto().size());		
		} catch (Exception e) {
			System.out.println("Error al listar datos");
            FacesMessage m = new FacesMessage("Error!", "No se puede obtener los datos");
            FacesContext.getCurrentInstance().addMessage(null, m);
			e.printStackTrace();
		}		
		return null;	
	}
	
	/**
	 * Metodo para seleccionar auto a rentar
	 * @return
	 */
	@SuppressWarnings("unlikely-arg-type")
	public String obtieneSelectedItems() {
		System.out.println("Entro metodo selecion multiple DataGrid");
		//Obtenemos los items seleccionados
		selectedDataList = new ArrayList<Auto>();	
		
		for(Auto dataItem : auto) {
			if(selectedIds.get(dataItem.getCodigo()).booleanValue()) {
				selectedDataList.add(dataItem);
				selectedIds.remove(dataItem.getCodigo());
//				codAutoSeleccionado= dataItem.getCodigo(); //Obtiene el codigo del Auto seleccionado para mostrar en la confirmacion de la Renta
//				modelo = dataItem.getModelo();
				numAsientos = dataItem.getNumero_asientos();
				redondear(valorTotalRentaPorDias = numeroDiasRenta * dataItem.getPrecio());
				System.out.println("Datos seleccionado: "+selectedIds + "Id seleccionado: "+dataItem.getCodigo()+ "Item selected: "+dataItem.getModelo());
			
//				System.out.println("LLeva usuario a reeeeeeeeeeeeeenta auto: "+usuarioSession);
				
				
				sesion.setAuto(dataItem); //Seteo el objeto Auto para guardar en la Renta
				sesion.setCantDiasRenta(numeroDiasRenta);
				sesion.setFechaGlobal(fechaRenta);
				sesion.setValorTotal(redondear(valorTotalRentaPorDias));			
				sesion.setHoraGlobal(sesion.getHoraGlobal());
				sesion.setNumeroAsientos(numAsientos);
				
				
				System.out.println("Model del auto seleccionado: "+sesion.getAuto().getModelo()+" ...........");
				
				System.out.println("Numero de dias por renta ingresado:   ///////"+numeroDiasRenta+"..............");
				System.out.println("Fecha de la rentaaaaaaaaaaaaaaaaaaa:   ///////"+fechaRenta+"..........");
				
				System.out.println("Hooooooooora de la reeeeeeeeeeeeeeenta:   ///////"+sesion.getHoraGlobal()+"..........");
				
				System.out.println("Valor total a pagar tarjeta"+sesion.getValorTotal()+".........................");
			}
		}
		return "confirmarRenta?faces-redirect=true";
		
	}
	
	/**
	 * Metodo para seleccionar auto a rentar
	 * @return
	 */
	@SuppressWarnings("unlikely-arg-type")
	public String obtieneSelectedItemsAutoEvento() {
		System.out.println("Entro metodo selecion multiple DataGrid Evento");
		//Obtenemos los items seleccionados
		selectedDataList = new ArrayList<Auto>();	
		
		for(Auto dataItem : auto) {
			if(selectedIds.get(dataItem.getCodigo()).booleanValue()) {
				selectedDataList.add(dataItem);
				selectedIds.remove(dataItem.getCodigo());
//				codAutoSeleccionado= dataItem.getCodigo(); //Obtiene el codigo del Auto seleccionado para mostrar en la confirmacion de la Renta
//				modelo = dataItem.getModelo();
//				redondear(valorTotalRentaPorDias = numeroDiasRenta * dataItem.getPrecio());
				System.out.println("Datos seleccionado: "+selectedIds + "Id seleccionado: "+dataItem.getCodigo()+ "Item selected: "+dataItem.getModelo());
			
//				System.out.println("LLeva usuario a reeeeeeeeeeeeeenta auto: "+usuarioSession);
				
				
				sesion.setAuto(dataItem); //Seteo el objeto Auto para guardar en la Renta
				sesion.setFechaGlobal(fechaRenta);
				sesion.setValorTotal(redondear(valorTotalRentaPorDias));			
				sesion.setHoraGlobal(sesion.getHoraGlobal());
				

				
				sesion.getChofer().getNombres();
				
				System.out.println("ChogerSeleccionado: "+ sesion.getChofer().getNombres()+"............");
				
				System.out.println("Model del auto seleccionado: "+sesion.getAuto().getModelo()+" ...........");

				System.out.println("Fecha de la rentaaaaaaaaaaaaaaaaaaa:   ///////"+fechaRenta+"..........");
				
				System.out.println("Hooooooooora de la reeeeeeeeeeeeeeenta:   ///////"+sesion.getHoraGlobal()+"..........");
				
				System.out.println("Valor total a pagar tarjeta"+sesion.getValorTotal()+".........................");
			}
		}
		return "confirmarEvento?faces-redirect=true";
		
	}
	
	
	/*
	 * Metodo para pasar a la ventana de Pago
	 */
	public String pagoRenta() {	
//		this.getCodAutoSeleccionado();
		this.getValorTotalRentaPorDias();
		this.getNumAsientos();
		
		System.out.println("Numero dias renta ///////"+numeroDiasRenta);
		
		return "pagoRenta?faces-redirec=true";
	}
	
	/**
	 * Actualizar datos a nivel de fila (Auto)
	 * @param event
	 * @throws Exception 
	 */
	 public void onRowEdit(RowEditEvent event) throws Exception {
		 Auto a = (Auto) event.getObject();
		 auBussiness.actualizar(a);
         FacesMessage msg = new FacesMessage("Auto actualizado", ""+((Auto) event.getObject()).getModelo());
         FacesContext.getCurrentInstance().addMessage(null, msg);
	    }
	 public void onRowCancel(RowEditEvent event) throws Exception {
		 Auto a = (Auto) event.getObject();
	 	 auBussiness.actualizar(a);
         FacesMessage msg = new FacesMessage("Actualizacion cancelada", ""+((Auto) event.getObject()).getModelo());
         FacesContext.getCurrentInstance().addMessage(null, msg);
	    }
	
	 /**
	  * Metodo para redondear Valor a Pagar
	  * @param numero
	  * @return
	  */
 	public  double redondear(double numero){
        // return Math.rint(numero*100)/100;
        return Math.round(numero*100.0)/100.0;
     }
	
// 	/*
// 	 * Metodo para calcular el precio
// 	 */
// 	public double precioTotalRenta() {
//		double precio = 0.0;
//			precio= precioAuto * numeroDiasRenta;
//		return precio;
//	}
//	 
	 
	 public Auto getNewAuto() {
		return newAuto;
	}

	public void setNewAuto(Auto newAuto) {
		this.newAuto = newAuto;
	}

	public List<Auto> getAuto() {
		return auto;
	}

	public void setAuto(List<Auto> auto) {
		this.auto = auto;
	}

	public Auto getSelectAuto() {
		return selectAuto;
	}

	public void setSelectAuto(Auto selectAuto) {
		this.selectAuto = selectAuto;
	}

	public Map<Long, Boolean> getSelectedIds() {
		return selectedIds;
	}

	public void setSelectedIds(Map<Long, Boolean> selectedIds) {
		this.selectedIds = selectedIds;
	}

	public List<Auto> getSelectedDataList() {
		return selectedDataList;
	}

	public void setSelectedDataList(List<Auto> selectedDataList) {
		this.selectedDataList = selectedDataList;
	}
	
//	public String getModelo() {
//		return modelo;
//	}
//
//	public void setModelo(String modelo) {
//		this.modelo = modelo;
//	}

	public int getNumAsientos() {
		return numAsientos;
	}

	public void setNumAsientos(int numAsientos) {
		this.numAsientos = numAsientos;
	}

//	public int getCodAutoSeleccionado() {
//		return codAutoSeleccionado;
//	}
//
//	public void setCodAutoSeleccionado(int codAutoSeleccionado) {
//		this.codAutoSeleccionado = codAutoSeleccionado;
//
//		}

	public Renta getNewRenta() {
		return newRenta;
	}

	public void setNewRenta(Renta newRenta) {
		this.newRenta = newRenta;
	}

//	public double getPrecioAuto() {
//		return precioAuto;
//	}
//
//	public void setPrecioAuto(double precioAuto) {
//		this.precioAuto = precioAuto;
//	}

	public int getNumeroDiasRenta() {
		return numeroDiasRenta;
	}

	public void setNumeroDiasRenta(int numeroDiasRenta) {
		this.numeroDiasRenta = numeroDiasRenta;
	}

	public double getValorTotalRentaPorDias() {
		return valorTotalRentaPorDias;
	}

	public void setValorTotalRentaPorDias(double valorTotalRentaPorDias) {
		this.valorTotalRentaPorDias = valorTotalRentaPorDias;
	}

	public Date getFechaRenta() {
		return fechaRenta;
	}

	public void setFechaRenta(Date fechaRenta) {
		this.fechaRenta = fechaRenta;
	}

//	public Cliente getUsuarioSession() {
//		return usuarioSession;
//	}
//
//	public void setUsuarioSession(Cliente usuarioSession) {
//		this.usuarioSession = usuarioSession;
//	}

	
	
	
}