package ec.edu.ups.appdis.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.validation.constraints.NotNull;

@Entity
//@PrimaryKeyJoinColumn(name="personaId")
public class Chofer implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	@Column(length=13, unique=true)
	@NotNull(message="ingresar cedula")
	private String cedula;
	
	@Column(length=100)
	@NotNull(message="ingresar nombre")
	private String nombres;
	
	@Column(length=100)
	@NotNull(message="ingresar apellido")
	private String apellidos;
	
	@Column(length=10)
	@NotNull(message="ingresar telefono")
	private String telefono;

	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="evento_chofer_id", referencedColumnName="codigo")
	private Set<Evento> evento = new HashSet<>();
	
	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="viaje_chofer_id", referencedColumnName="codigo")
	private Set<Viaje> viaje = new HashSet<>();
	
	
	
	
	/*
	 * Generate Getters and Setters
	 */
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getCedula() {
		return cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public Set<Evento> getEvento() {
		return evento;
	}

	public void setEvento(Set<Evento> evento) {
		this.evento = evento;
	}

	public Set<Viaje> getViaje() {
		return viaje;
	}

	public void setViaje(Set<Viaje> viaje) {
		this.viaje = viaje;
	}
		
}
