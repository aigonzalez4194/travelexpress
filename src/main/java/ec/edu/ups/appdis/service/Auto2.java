package ec.edu.ups.appdis.service;


public class Auto2{
	
	private int codigo;
	private String modelo;
	private String placa;
	private String imagen_auto;
	private int numeroAsientos;
	
	
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getImagen_auto() {
		return imagen_auto;
	}
	public void setImagen_auto(String imagen_auto) {
		this.imagen_auto = imagen_auto;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public int getNumeroAsientos() {
		return numeroAsientos;
	}
	public void setNumeroAsientos(int numeroAsientos) {
		this.numeroAsientos = numeroAsientos;
	}
	
}
