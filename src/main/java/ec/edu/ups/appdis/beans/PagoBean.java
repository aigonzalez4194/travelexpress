package ec.edu.ups.appdis.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import ec.edu.ups.appdis.bussiness.PagoBussiness;
import ec.edu.ups.appdis.bussiness.TipoBussiness;
import ec.edu.ups.appdis.model.Pago;
import ec.edu.ups.appdis.model.Tipo_Pago;


@Named("bean_pago")
@ViewScoped
public class PagoBean implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Inject
	private TipoBussiness tpagoBussiness;
	
	@Inject
	private PagoBussiness pagoBussiness;
	
	@Inject
	private FacesContext facesContext;
	
	private Pago newPago;
	private List<Tipo_Pago> tpagos;
	
	@Inject 
	private Sesion sesion;
	
	
	/*
	 * Variables para cargar el tipo de pago al Combo, Pago
	 */
	private List<Tipo_Pago> listaTipoPago;
	private List<SelectItem> listadoTipoPagoCombo;
	private String selecionTipoPago;
	
	@PostConstruct
	public void init() {
		
		newPago=new Pago();
		//tpagos = perBussiness.getListadoTPago();	\
//		
//		auto.getUsuarioSession();
//		System.out.println("Usuario obteniiiiiiiiiiiiiido PAGO: "+auto.getUsuarioSession().getCodigo());
		
		cargarSeleccionTipoPago();
	}
	

	public Pago guardar() {
		try {
			pagoBussiness.save(newPago);
			
			sesion.setPago(newPago);
			System.out.println("Registro Guardado");
			
			return newPago;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Error al guardar");
			 
	            FacesMessage m = new FacesMessage(
	            		FacesMessage.SEVERITY_INFO, "Error!", e.getMessage());
	            facesContext.addMessage(null, m);
			e.printStackTrace();
		}
		return null ;
		
	}
	
	public String Eliminar (int cod )throws Exception {
		try {
			System.out.println("Entro metodo eliminar");
			pagoBussiness.eliminar(cod);
			init();
			FacesMessage msg = new FacesMessage("Aviso","Un elemento ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Error","El registro no ha sido eliminado");
	        FacesContext.getCurrentInstance().addMessage(null, msg);
		}		
		return null;
	}
	
	/**
	 * Metodo para cargar (tipoPago) a la lista del combo.
	 */
	public void cargarSeleccionTipoPago (){
		listadoTipoPagoCombo = new ArrayList<SelectItem>();
		listaTipoPago=tpagoBussiness.getListadoTPago();
		for(int i=0; i<listaTipoPago.size(); i++){
			listadoTipoPagoCombo.add(new SelectItem(listaTipoPago.get(i).getCodigo(),listaTipoPago.get(i).getDescripcion()));
//			System.out.println("--------Carga categorias opcion--------");
		}
	}
	
	
	public void seleccionarTipoPagoCombo(){
		try {
			for (int i=0; i<listaTipoPago.size(); i++){
				if((""+listaTipoPago.get(i).getCodigo()).equals(selecionTipoPago)){
					newPago.setTpago(listaTipoPago.get(i));
					//return String.valueOf(listaTipoPago.get(i).getCodigo()) ;
					System.out.println("Codigo tipoPago: "+ listaTipoPago.get(i).getCodigo()+ " ==> "+ "Nombre: "+listaTipoPago.get(i).getDescripcion());
				}	
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
//	public String seleccionarTipoPagoCombo(){
//		try {
//			for (int i=0; i<listaTipoPago.size(); i++){
//				if((""+listaTipoPago.get(i).getCodigo()).equals(selecionTipoPago)){
//					//newPago.setTpago(listaTipoPago.get(i));
//					return String.valueOf(listaTipoPago.get(i).getCodigo()) ;
//					//System.out.println("Codigo tipoPago: "+ listaTipoPago.get(i).getCodigo()+ " ==> "+ "Nombre: "+listaTipoPago.get(i).getDescripcion());
//				}	
//			}
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	
	/**
	 * Actualizar datos a nivel de fila (Tipo de Pago)
	 * @param event
	 * @throws Exception 
	 */
	 public void onRowEdit(RowEditEvent event) throws Exception {
		 Pago tp = (Pago) event.getObject();
		 pagoBussiness.actualizar(tp);
         FacesMessage msg = new FacesMessage("Cliente actualizado", ""+((Pago) event.getObject()).getCodigo());
         FacesContext.getCurrentInstance().addMessage(null, msg);
	    }
	 public void onRowCancel(RowEditEvent event) throws Exception {
		Pago tp = (Pago) event.getObject();
		pagoBussiness.actualizar(tp);
        FacesMessage msg = new FacesMessage("Actualizacion cancelada", ""+((Pago) event.getObject()).getCodigo());
        FacesContext.getCurrentInstance().addMessage(null, msg);
	    }

	
	/*
	 * Getters and Setters
	 */

	public Pago getNewPago() {
		return newPago;
	}


	public void setNewPago(Pago newPago) {
		this.newPago = newPago;
	}


	public List<Tipo_Pago> getTpagos() {
		return tpagos;
	}

	public void setTpagos(List<Tipo_Pago> tpagos) {
		this.tpagos = tpagos;
	}


	public List<Tipo_Pago> getListaTipoPago() {
		return listaTipoPago;
	}


	public void setListaTipoPago(List<Tipo_Pago> listaTipoPago) {
		this.listaTipoPago = listaTipoPago;
	}


	public List<SelectItem> getListadoTipoPagoCombo() {
		return listadoTipoPagoCombo;
	}


	public void setListadoTipoPagoCombo(List<SelectItem> listadoTipoPagoCombo) {
		this.listadoTipoPagoCombo = listadoTipoPagoCombo;
	}


	public String getSelecionTipoPago() {
		return selecionTipoPago;
	}


	public void setSelecionTipoPago(String selecionTipoPago) {
		this.selecionTipoPago = selecionTipoPago;
	}


	public Sesion getSesion() {
		return sesion;
	}


	public void setSesion(Sesion sesion) {
		this.sesion = sesion;
	}
	
}
