package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.model.Pago;
import ec.edu.ups.appdis.model.Tipo_Pago;

@Stateless
public class PagoDao {
	
	@Inject
	private EntityManager em;
	
	public void insert(Pago pag ) {	
		em.persist(pag);
		}
	
	public void update(Pago pag){
		em.merge(pag);
		
	}
	public void remove(int id){
		em.remove(read(id));
		
	}
	public Pago read(int id){
		Pago aux=em.find(Pago.class, id);
		return aux;
		  
	}
	
	public List<Pago>getPagos(){
		
		String jpaql="Select p from Pago p";	
		Query query=em.createQuery(jpaql, Pago.class);
		List<Pago>listar=query.getResultList();
		return listar;
	}
	

}
