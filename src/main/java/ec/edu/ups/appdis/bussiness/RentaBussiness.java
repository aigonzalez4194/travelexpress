package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import ec.edu.ups.appdis.dao.RentaDao;
import ec.edu.ups.appdis.model.Renta;


@Stateless
public class RentaBussiness {
	

	@Inject
	private RentaDao cliDao;
	private List<Renta> persona;
	
	
	public boolean save(Renta cliente)throws Exception {
		
		Renta cli=cliDao.read(cliente.getCodigo());
		if (cli!=null) 
			cliDao.update(cliente);
		else 
			
			cliDao.insert(cliente);
			
		return true;		
	}	
	
	public List<Renta>getListadoRentas(){
		
		return cliDao.getRentas();
	}
	
	public List<Renta> getCliente() {
		return persona;
	}

	
	public void eliminar (int cod) throws Exception {
		Renta aux=cliDao.read(cod);       // el aux es igual al per
		if(aux==null)throw new Exception("Persona No Exixte");
		else 
			cliDao.remove(cod);
		
	}
	
	public Renta readAuto(int id) {
		return cliDao.read(id);
	}
	
	public void actualizar(Renta  cliente)throws Exception {
		Renta aux=cliDao.read( cliente.getCodigo());       // el aux es igual al per
		if(aux==null)throw new Exception("Persona No Exixte");
		else 
			cliDao.update( cliente);
	}
	
	public List<Renta> obtenerHistorial(String user){
		List<Renta> consultaHistorial = cliDao.historialCliente(user);
		return consultaHistorial;
	}

}
