package ec.edu.ups.appdis.dao;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import ec.edu.ups.appdis.model.eventoApp;
import ec.edu.ups.appdis.model.rentaApp;
import ec.edu.ups.appdis.model.viajeApp;

@Stateless
public class AppDaoTE {
	
	@Inject
	private EntityManager em;
	
	public void guardarRenta(rentaApp ra) {
		
		em.persist(ra);
	}
	
	public void guardarViaje (viajeApp va) {
		
		em.persist(va);
	}
	
	
	public void guardarEvento (eventoApp e) {
		em.persist(e);
	}
}
