package ec.edu.ups.appdis.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Renta {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	@NotNull
	private Date fecha;
	
	@NotNull
	@Column(length=50)
	private String hora;
	
	@NotNull
	private int numeroDias;
	
	@NotNull
	@Column(length=15)
	private String estado;

	/*
	 * Relaciones JPA
	 */
	@ManyToOne
	@JoinColumn(name="cliente_renta_id", nullable=false)
	@JsonIgnore
	private Cliente cliente;
	
	@ManyToOne
	@JoinColumn(name="renta_auto_id", nullable=false)
	@JsonIgnore
	private Auto auto;
	
	@ManyToOne
	@JoinColumn(name="renta_pago_id", nullable=false)
	@JsonIgnore
	private Pago pago;
	
	
	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public int getNumeroDias() {
		return numeroDias;
	}

	public void setNumeroDias(int numeroDias) {
		this.numeroDias = numeroDias;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Auto getAuto() {
		return auto;
	}

	public void setAuto(Auto auto) {
		this.auto = auto;
	}

	public Pago getPago() {
		return pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	
}
