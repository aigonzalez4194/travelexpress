package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import ec.edu.ups.appdis.dao.ChoferDao;
import ec.edu.ups.appdis.model.Chofer;


@Stateless
public class ChoferBussiness {

	
	@Inject
	private ChoferDao chofDao;
	private List<Chofer> persona;
	
	
	public boolean save(Chofer cliente)throws Exception {
		
		Chofer cli=chofDao.read(cliente.getCodigo());
		if (cli!=null) 
			chofDao.update(cliente);
		else 			
			chofDao.insert(cliente);		
		return true;
		
	}
		
	public List<Chofer>getListadoChofer(){
		
		return chofDao.getChofer();
	}
	
	public List<Chofer> getCliente() {
		return persona;
	}

	
	public void eliminar (int cod) throws Exception {
		Chofer aux=chofDao.read(cod);       // el aux es igual al per
		if(aux==null)throw new Exception("Chofer No Exixte");
		else 
			chofDao.remove(cod);
		
	}
	
	public void actualizar(Chofer  cliente)throws Exception {
		Chofer aux=chofDao.read( cliente.getCodigo());       // el aux es igual al per
		if(aux==null)throw new Exception("Chofer No Exixte");
		else 
			chofDao.update( cliente);
	}
	
}
