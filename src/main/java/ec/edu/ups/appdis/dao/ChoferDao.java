package ec.edu.ups.appdis.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.model.Chofer;



@Stateless
public class ChoferDao {
	
	@Inject
	private EntityManager em;
	
	
	public void insert(Chofer chofer ) {
		
		em.persist(chofer);
		}
	
	public void update(Chofer chofer){
		em.merge(chofer);
		
	}
	public void remove(int id){
		em.remove(read(id));
		
	}
	public Chofer read(int id){
		Chofer aux=em.find(Chofer.class, id);
		return aux;
		  
	}
	
	public List<Chofer>getChofer(){
		
		String jpaql="Select ch from Chofer ch";	
		Query query=em.createQuery(jpaql,Chofer.class);
		List<Chofer>listar=query.getResultList();
		return listar;
	}
	
	

}
