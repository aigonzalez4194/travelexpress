package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.PagoDao;
import ec.edu.ups.appdis.dao.TipoDao;
import ec.edu.ups.appdis.model.Pago;
import ec.edu.ups.appdis.model.Tipo_Pago;

@Stateless
public class PagoBussiness {
	
	@Inject
	private PagoDao daoPago;
	private List<Pago> pagos;
	
	
	public boolean save(Pago tpago)throws Exception {
		
		Pago pag=daoPago.read(tpago.getCodigo());
		if (pag!=null) 
			daoPago.update(tpago);
		else 			
			daoPago.insert(tpago);
			
		return true;
		
	}
	
	public void guardar(Pago pag) {
		daoPago.insert(pag);
	}
	
	public List<Pago>getListadoPagos(){
		
		return daoPago.getPagos();
	}
		
	public void eliminar (int cod) throws Exception {
		Pago aux=daoPago.read(cod);       // el aux es igual al per
		if(aux==null)throw new Exception("Pago No Exixte");
		else 
			daoPago.remove(cod);
		
	}
	
	public void actualizar(Pago  pag)throws Exception {
		Pago aux=daoPago.read( pag.getCodigo());       // el aux es igual al per
		if(aux==null)throw new Exception("Pago No Exixte");
		else 
			daoPago.update( pag);
	}
	

}
