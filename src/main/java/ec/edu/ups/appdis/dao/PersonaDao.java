package ec.edu.ups.appdis.dao;


import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import ec.edu.ups.appdis.model.Persona;



@Stateless
public class PersonaDao {
	
	@Inject
	private EntityManager em;
	
	public void insert(Persona persona ) {
		
		em.persist(persona);
		
		}
	
	public void update(Persona persona){
		em.merge(persona);
		
	}
	public void remove(String id){
		em.remove(read(id));
		
	}
	public Persona read(String id){
		Persona aux=em.find(Persona.class, id);
		return aux;
		  
	}
	
	/**
	 * Metodo para listar Personas
	 * @return
	 */
	public List<Persona>getPersona(){
		
		String jpaql="Select p from Persona p";	
		Query query=em.createQuery(jpaql,Persona.class);
		List<Persona>listar=query.getResultList();
		return listar;
	}
	
	

}
