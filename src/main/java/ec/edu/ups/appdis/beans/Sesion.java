package ec.edu.ups.appdis.beans;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import ec.edu.ups.appdis.model.Auto;
import ec.edu.ups.appdis.model.Chofer;
import ec.edu.ups.appdis.model.Cliente;
import ec.edu.ups.appdis.model.Pago;
import ec.edu.ups.appdis.model.Tipo_Pago;

import java.io.Serializable;
import java.util.Date;


@Named("sesion")
@SessionScoped
public class Sesion implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	public Sesion () {
		
	}
	/*
	 * Instancia de Objetos para recuperar sus atributos al Inyectar en el Bean que necesitemos
	 */
	private Cliente user;
	
	private String usuario;
	
	private Cliente cliente;
	
	private Auto auto;
	
	private Pago pago;
	
	private Tipo_Pago tipoPago;
	
	private Chofer chofer;
	
	
	
	/*
	 * Variables para manejar (Renta, Viaje y Evento)
	 */
	private int cantDiasRenta;
	
	private Date fechaGlobal;
	
	private String horaGlobal;
	
	private double valorTotal;
	
	private int numeroAsientos;
	

	/*
	 * Getters and Setters
	 */
	public Cliente getUser() {
		return user;
	}

	public void setUser(Cliente user) {
		this.user = user;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Auto getAuto() {
		return auto;
	}

	public void setAuto(Auto auto) {
		this.auto = auto;
	}

	public Pago getPago() {
		return pago;
	}

	public void setPago(Pago pago) {
		this.pago = pago;
	}

	public Tipo_Pago getTipoPago() {
		return tipoPago;
	}

	public void setTipoPago(Tipo_Pago tipoPago) {
		this.tipoPago = tipoPago;
	}

	public int getCantDiasRenta() {
		return cantDiasRenta;
	}

	public void setCantDiasRenta(int cantDiasRenta) {
		this.cantDiasRenta = cantDiasRenta;
	}

	public Date getFechaGlobal() {
		return fechaGlobal;
	}

	public void setFechaGlobal(Date fechaGlobal) {
		this.fechaGlobal = fechaGlobal;
	}

	public String getHoraGlobal() {
		return horaGlobal;
	}

	public void setHoraGlobal(String horaGlobal) {
		this.horaGlobal = horaGlobal;
	}

	public double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Chofer getChofer() {
		return chofer;
	}

	public void setChofer(Chofer chofer) {
		this.chofer = chofer;
	}

	public int getNumeroAsientos() {
		return numeroAsientos;
	}

	public void setNumeroAsientos(int numeroAsientos) {
		this.numeroAsientos = numeroAsientos;
	}

	
}
