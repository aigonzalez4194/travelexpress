package ec.edu.ups.appdis.bussiness;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.appdis.dao.AutoDao;
import ec.edu.ups.appdis.dao.EventoDao;
import ec.edu.ups.appdis.dao.RentaDao;
import ec.edu.ups.appdis.model.Auto;
import ec.edu.ups.appdis.model.Evento;
import ec.edu.ups.appdis.model.Renta;


@Stateless
public class EventoBussiness {
	

	@Inject
	private EventoDao cliDao;
	private List<Evento> eventos;
	
	
	public boolean save(Evento cliente)throws Exception {
		
		Evento cli=cliDao.read(cliente.getCodigo());
		if (cli!=null) 
			cliDao.update(cliente);
		else 
			
			cliDao.insert(cliente);
			
		return true;
		
	}
	
	public List<Evento>getListadoEventos(){
		
		return cliDao.getEventos();
	}
	
//	public List<Renta> getCliente() {
//		return eventos;
//	}

	
	public void eliminar (int cod) throws Exception {
		Evento aux=cliDao.read(cod);       // el aux es igual al per
		if(aux==null)throw new Exception("Evento No Exixte");
		else 
			cliDao.remove(cod);
		
	}
	
	public Evento readEvento(int id) {
		return cliDao.read(id);
	}
	
	public void actualizar(Evento  cliente)throws Exception {
		Evento aux=cliDao.read( cliente.getCodigo());       // el aux es igual al per
		if(aux==null)throw new Exception("Persona No Exixte");
		else 
			cliDao.update( cliente);
	}
	

}
