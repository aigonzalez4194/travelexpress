package ec.edu.ups.appdis.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Pago {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	@NotEmpty
	@Column(name="p_Nombre_Tarjeta", length=100)
	private String nombreTarjeta;
	
	@NotEmpty
	@Column(length=16, name="p_Numero_Tarjeta")
	@Size(min=16, max=16)
	private String numeroTarjeta;
	
	
	@NotEmpty
	@Column(name="p_Fecha_Expiracion_Tarjeta_Mes", length=5)
	@Size(min = 2, max = 2)
	private String fechaExpiracionTarjetaMes;
	
	@NotEmpty
	@Column(name="p_Fecha_Expiracion_Tarjeta_Año", length=5)
	@Size(min = 4, max = 4)
	private String fechaExpiracionTarjetaAño;
	
	@NotEmpty
	@Size(min=3, max=3)
	@Column(name="p_Codigo_Seguridad", length=5)
	private String codigoSeguridad;

//	@NotEmpty
//	@Column(precision=8)
//	@Size(min=10, max=50)
//	private double valorTotal;

	

	@ManyToOne
	@JoinColumn(name="pago_tpago_id", nullable=false)
	private Tipo_Pago tpago;

	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="renta_pago_id", referencedColumnName="codigo")
	private Set<Renta> rentas = new HashSet<>();
	
	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="viaje_pago_id", referencedColumnName="codigo")
	private Set<Viaje> viajes = new HashSet<>();
	
	@OneToMany(cascade= {CascadeType.ALL})
	@JoinColumn(name="evento_pago_id", referencedColumnName="codigo")
	private Set<Evento> eventos = new HashSet<>();
	

	public int getCodigo() {
		return codigo;
	}


	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNombreTarjeta() {
		return nombreTarjeta;
	}


	public void setNombreTarjeta(String nombreTarjeta) {
		this.nombreTarjeta = nombreTarjeta;
	}


	public String getFechaExpiracionTarjetaMes() {
		return fechaExpiracionTarjetaMes;
	}


	public void setFechaExpiracionTarjetaMes(String fechaExpiracionTarjetaMes) {
		this.fechaExpiracionTarjetaMes = fechaExpiracionTarjetaMes;
	}


	public String getFechaExpiracionTarjetaAño() {
		return fechaExpiracionTarjetaAño;
	}


	public void setFechaExpiracionTarjetaAño(String fechaExpiracionTarjetaAño) {
		this.fechaExpiracionTarjetaAño = fechaExpiracionTarjetaAño;
	}


	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}


	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}


	public Tipo_Pago getTpago() {
		return tpago;
	}


	public void setTpago(Tipo_Pago tpago) {
		this.tpago = tpago;
	}
	
	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}


	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}


	public Set<Renta> getRentas() {
		return rentas;
	}


	public void setRentas(Set<Renta> rentas) {
		this.rentas = rentas;
	}


	public Set<Viaje> getViajes() {
		return viajes;
	}


	public void setViajes(Set<Viaje> viajes) {
		this.viajes = viajes;
	}


	public Set<Evento> getEventos() {
		return eventos;
	}


	public void setEventos(Set<Evento> eventos) {
		this.eventos = eventos;
	}

//
//	public double getValorTotal() {
//		return valorTotal;
//	}
//
//
//	public void setValorTotal(double valorTotal) {
//		this.valorTotal = valorTotal;
//	}
//	
	
}
