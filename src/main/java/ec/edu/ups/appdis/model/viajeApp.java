package ec.edu.ups.appdis.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class viajeApp {

	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	private String usuario;
	
	private String modelo;
	
	private String asientosViaje;
	
	private String valorViaje;
	
	private String fecha;
	
	private String hora;
	
	private String lugar;
	
	private String formaPago;
	
	private String titularT;
	
	private String numeroT;
	
	private String anioT;
	
	private String codigoT;

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getAsientosViaje() {
		return asientosViaje;
	}

	public void setAsientosViaje(String asientosViaje) {
		this.asientosViaje = asientosViaje;
	}

	public String getValorViaje() {
		return valorViaje;
	}

	public void setValorViaje(String valorViaje) {
		this.valorViaje = valorViaje;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getLugar() {
		return lugar;
	}

	public void setLugar(String lugar) {
		this.lugar = lugar;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getTitularT() {
		return titularT;
	}

	public void setTitularT(String titularT) {
		this.titularT = titularT;
	}

	public String getNumeroT() {
		return numeroT;
	}

	public void setNumeroT(String numeroT) {
		this.numeroT = numeroT;
	}

	public String getCodigoT() {
		return codigoT;
	}

	public void setCodigoT(String codigoT) {
		this.codigoT = codigoT;
	}

	public String getAnioT() {
		return anioT;
	}

	public void setAnioT(String anioT) {
		this.anioT = anioT;
	}
	
}
