package ec.edu.ups.appdis.beans;

import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import org.primefaces.event.RowEditEvent;
import ec.edu.ups.appdis.bussiness.LugarBussiness;
import ec.edu.ups.appdis.model.Lugar;


@Named("bean_lugar")
@ViewScoped
public class LugarBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	String version = FacesContext.class.getPackage().getImplementationVersion();
	
	@Inject
	FacesContext facesContext;
	
	@Inject
	private LugarBussiness lgrBussiness;
	
	private Lugar newLugar;
	private List<Lugar> lugares;
	
	private String demo;
	
	@PostConstruct
	public void init() {
		newLugar=new Lugar();
		lugares = lgrBussiness.getListadoLugares();
	
	}
	
	public String versionJSF() {
		System.out.println("Version JSF es: "+version);
		return version;
	}
	
	public void limpiarFormulario() {
		newLugar.setNombre("");
		newLugar.setPrecio(0);
	}
	
	public String guardar() {	
		try {
			lgrBussiness.save(newLugar);
			System.out.println("Registro Guardado");
			FacesMessage msg = new FacesMessage("Nuevo lugar registrado");
	        facesContext.addMessage(null, msg);
			//return null;
		} catch (Exception e) {		
			System.out.println("No se puede insertar el registro");
			FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, e.getMessage(), "Error al guardar");
//			FacesMessage msg = new FacesMessage("Error de registro");
//	        FacesContext.getCurrentInstance().addMessage(null, msg);
	        facesContext.addMessage(null, msg);
	        e.printStackTrace();
		}finally {
			limpiarFormulario();
		}
		return null;	
	}

	public String eliminar (int cod) {
		System.out.println("Entro metodo eliminar");
		try {
			lgrBussiness.eliminar(cod);
			init();
			FacesMessage msg = new FacesMessage("Aviso","Un elemento ha sido eliminado");
	        facesContext.addMessage(null, msg);
		} catch (Exception e) {
			e.printStackTrace();
			FacesMessage msg = new FacesMessage("Error","El registro no ha sido eliminado");
	        facesContext.addMessage(null, msg);
		}
		return "";
	}
	
	/**
	 * Metodo para listar los  lugares registrados
	 * @return
	 */
//	public String ListarLugares() {
//		try {
//			lugares = lgrBussiness.getListadoLugares();
//			System.out.println("Datos encontrados"+" " + getLugares().size());		
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			System.out.println("Error al guardar");
//			 
//	            FacesMessage m = new FacesMessage(
//	            		FacesMessage.SEVERITY_INFO, "Error!", e.getMessage());
//	            facesContext.addMessage(null, m);
//			e.printStackTrace();
//		}		
//		return null;	
//	}
	
	/**
	 * Actualizar datos a nivel de fila (Lugar)
	 * @param event
	 * @throws Exception 
	 */
	 public void onRowEdit(RowEditEvent event) throws Exception {
		 Lugar l = (Lugar) event.getObject();
		 lgrBussiness.actualizar(l);
         FacesMessage msg = new FacesMessage("Lugar actualizado", ""+((Lugar) event.getObject()).getNombre());
         facesContext.addMessage(null, msg);
	    }
	 public void onRowCancel(RowEditEvent event) throws Exception {
	    Lugar cli = (Lugar) event.getObject();
	 	lgrBussiness.actualizar(cli);
        FacesMessage msg = new FacesMessage("Actualizacion cancelada", ""+((Lugar) event.getObject()).getNombre());
        facesContext.addMessage(null, msg);
	    }
	
	public Lugar getNewLugar() {
		return newLugar;
	}

	public void setNewLugar(Lugar newLugar) {
		this.newLugar = newLugar;
	}

	public List<Lugar> getLugares() {
		return lugares;
	}

	public void setLugares(List<Lugar> lugares) {
		this.lugares = lugares;
	}

	public String getDemo() {
		return demo;
	}

	public void setDemo(String demo) {
		this.demo = demo;
	}
	
	
}
