package ec.edu.ups.appdis.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Tipo_Pago {
	
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	private int codigo;
	
	@Column(length=50)
	@NotNull
	private String descripcion;

	@OneToMany(cascade= {CascadeType.ALL},fetch=FetchType.LAZY)
	@JoinColumn(name="pago_tpago_id" ,referencedColumnName="codigo")
	private Set<Pago> pagos = new HashSet<>();
	
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public Set<Pago> getPagos() {
		return pagos;
	}
	public void setPagos(Set<Pago> pagos) {
		this.pagos = pagos;
	}
	
	

}
